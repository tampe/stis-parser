;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language c-calc spec)
  #:use-module (parser stis-parser lang C-parser)
  #:use-module (system base compile)
  #:use-module (system base language)
  #:use-module (language scheme compile-tree-il)
  #:use-module (language scheme decompile-tree-il)
  #:use-module (ice-9 rdelim)
  #:export (calc))

;;;
;;; Language definition
;;;

(define-language c-calc
  #:title	"C-calc"
  #:reader      (lambda (port env)
                  ;; Use the binding of current-reader from the environment.
                  ;; FIXME: Handle `read-options' as well?
                  (c (read-line port)))

  #:compilers   `((tree-il . ,compile-tree-il))
  #:decompilers `((tree-il . ,decompile-tree-il))
  #:evaluator	(lambda (x module) (primitive-eval x))
  #:printer	write
  #:make-default-environment
                (lambda ()
                  ;; Ideally we'd duplicate the whole module hierarchy so that `set!',
                  ;; `fluid-set!', etc. don't have any effect in the current environment.
                  (let ((m (make-fresh-user-module)))
                    ;; Provide a separate `current-reader' fluid so that
                    ;; compile-time changes to `current-reader' are
                    ;; limited to the current compilation unit.
                    (module-define! m 'current-reader (make-fluid))

                    ;; Default to `simple-format', as is the case until
                    ;; (ice-9 format) is loaded.  This allows
                    ;; compile-time warnings to be emitted when using
                    ;; unsupported options.
                    (module-set! m 'format simple-format)

                    m)))
