;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   
   (guile
    (define-module name args ...))))


 (define-module- (parser stis-parser)
  #:use-module (parser stis-parser scanner)
  #:use-module (parser stis-parser macros)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 format)
  #:use-module (parser stis-parser fstream)
  #:use-module (parser stis-parser slask)
  #:use-module (rnrs bytevectors)
  #:use-module (system foreign)  
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 pretty-print)
  #:export (pmatch plambda X XL N M <p-define> <p-lambda> .. xx <p-cc>
		   s-scan-item
		   f-test f-test2 f-test2! f-test!  f-char f-read
		   f-char!
		   clear-tokens mk-token mk-token-raw tok-ws* tok-ws* Ds
		   s* s+ fn f< f> f-pk f-pkk f-do-nl
		   mk-simple-token p-freeze
		   make-file-reader file-next-line file-skip
		   pr-char pr-test pr-reg
		   s-tag s-tag! pr-tag s-clear-body pr-not
		   s-rpl f-rpl s-tr f-tr
		   f-and f-and! f-and!! 
		   f-or  f-or! f-not* f-not f-not!
		   f-seq f-seq! f-seq!!
		   f-nm f* f+ f? f-tag f-tag! f-tag-pr
                   g* g+ g? gg* gg+ gg? gmn ngmn
		   ng* ng+ ng?
		   f-reg f-reg! f-reg-pr f-skip
		   f-false f-true f-id
		   f-eof f-nl f-nl! f-nl-pr
		   f-line f-cons f-list f-append f-cons* ff? ff* ff+ f-out
                   f-addchar
		   parse parse-raw parse-no-clear
		   *current-file-parsing*
		   *translator* *whitespace*
		   setup-parser
		   f-clear-body f-seek
		   f-ichar f-collect f-let f-n f-m
                   f-if f-if! f-cond f-cond! f-check f-checkr D
                   f-scope f-rev f-prev f-ftr f-pos f-seek f-maxlen
                   f-stepped))

(define oldpos (make-fluid -1))

(define vhash-ref
  (case-lambda
   ((vh x er)
    (let ((r (vhash-assoc x vh)))
      (if r
	  (cdr r)
	  er)))
   ((vh x)
    (vhash-ref vh x #f))))

(define *translator*           (make-fluid (lambda (x) x)))
(define *current-file-parsing* (make-fluid 'repl))
(define *whitespace* (make-fluid (lambda (s p cc . x) (apply cc s p x))))

(define do-print (slask-it #f))
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (let ()
         (define port (open-file "/home/stis/src/python-on-guile/log.txt" "a"))
         (with-output-to-port port
           (lambda ()
             (pretty-print `(,s ,(syntax->datum x)))))
         (close port)))
     x)
    ((x)
     (when do-print
       (let ()
         (define port (open-file "/home/stis/src/python-on-guile/log.txt" "a"))
         (with-output-to-port port
           (lambda ()
             (pretty-print (syntax->datum x))))
         (close port)))
     x)))

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define-syntax-rule (Ds f) (lambda x (apply f x)))

(define rawdir (make-fluid 1))
(define-inlinable (Di) (fluid-ref rawdir))

(define *freeze-map*   #f)

(define clear-tokens
  (let ((fl (make-fluid vlist-null)))
    (set! *freeze-map* fl)
    (lambda ()
      (fluid-set! oldpos (list 0 1))
      (fluid-set! rawdir 1)
      (let ((ht vlist-null))
	(fluid-set! fl ht)))))

(clear-tokens)

(define dynlink (dynamic-link))

(define stringn
  (pointer->procedure
   '*
   (dynamic-func "scm_from_latin1_stringn" dynlink)
   (list '* size_t)))

(define (chf ch)
  (let ((bytes (pointer->scm
                (stringn
                 (bytevector->pointer
                  (let ((b (make-bytevector 1)))
                    (bytevector-u8-set! b 0 ch)
                    b))
                 1))))
    (if (= (string-length bytes) 1)
        (string-ref bytes 0)
        (chf 0))))

(define (repr b)
  (let ((ch b))
    (cond
     ((equal? ch 0)
      "\\x00")
     ((equal? (chf ch) #\\)
      "\\\\")
     ((equal? (chf ch) #\')
      "\\'")
     ((equal? (chf ch) #\newline)
      "\\n")
     ((= ch 7)
      "\\a")
     ((= ch 8)
      "\\b")
     ((= ch 12)
      "\\f")
     ((= ch 10)
      "\\n")
     ((= ch 13)
      "\\r")
     ((= ch 9)
      "\\t")
     ((= ch 11)
      "\\v")
     (else
      (if (< ch 32)		  
          (format #f "\\x~2,'0x" ch)
          (make-string 1 (chf ch)))))))

(define-inlinable (btr x)
  (if (char? x)
      x
      (if (and (number? x) (integer? x))
          (if (< x 128)
              (integer->char x)
              (string-ref (repr x) 0)))))


(define (raws? x) (or (vector? x) (bytevector? x)))


(define-inlinable (bytevector-ref v i) (bytevector-u8-ref v i))

(define-inlinable (start-x x)
  (if (raws? x)
      0
      '()))

(define-inlinable (start-m x m)
  (if (raws? x)
      (+ m 1)
      m))

(define-inlinable (len x)
  (if (raws? x)
      (if (bytevector? x)
	  (bytevector-length x)
	  (vector-length x))
      0))

(define maxlen (make-fluid -1))

(define-inlinable (feof2? x xl)
  (let ((n (fluid-ref maxlen)))
    (if (< n 0) (set! n 1000000000))
    (if (raws? xl)
        (or (>= x (min n (len xl))) (< x 0))
        (and (null? x) (feof? xl)))))

(define inl (char->integer #\newline))
(define-inlinable (next-char? x xl)
  (if (raws? xl)
      (and (not (feof2? x xl))
           (cond
            ((bytevector? xl)
             (not (= (bytevector-ref xl x) inl)))
            (else
             (not (equal? (vector-ref xl x) #\newline)))))
      (pair? x)))

(define-inlinable (first x xl)
  (if (raws? xl)
      (cond
       ((bytevector? xl)
        (bytevector-ref xl x))
       (else
        (vector-ref xl x)))
      (car x)))

(define-inlinable (next x xl)
  (if (raws? xl)
      (+ x (Di))
      (cdr x)))


(define (lpp ii xx rr xxl)
  (if (= ii 0)
      (reverse rr)
      (if (raws? xxl)
	  (if (next-char? xx xxl)
	      (cond
	       ((bytevector? xxl)
		(lpp (- ii 1) (+ xx 1) (cons (repr (bytevector-ref xxl xx)) rr) xxl))
	       (else
		(lpp (- ii 1) (+ xx 1) (cons (vector-ref xxl xx) rr) xxl)))
	      (reverse rr))
	  (if (pair? xx)
	      (lpp (- ii 1) (cdr xx) (cons (car xx) rr) xxl)
	      (reverse rr)))))

(define (head-n n x xl)
  (lpp n x '() xl))


(define make-file-reader 
  (case-lambda
    ((stream)
     (cond
      ((raws? stream)
       stream)            
      ((port? stream)
       (rw-fstream-from-port stream))
      (else
       (let ((s (fluid-ref stream)))
         (cond
          ((fstream-rw? s)
           s)
          ((fstream-r? s)
           (r->rw-fstream s)))))))
    (()
     (make-file-reader (current-input-port)))))

(define (print-last-line x n xl)
  (when (and do-print (not (feof2? x xl)))
    (if (raws? xl)
        (format #t "endline (~a) : ~a~%" n
                (cond
                 ((bytevector? xl)
                  (let ((n (len xl)))
                    (let lp ((i x) (r '()))
                      (if (< i n)
                          (lp (+ i 1) (cons (repr (bytevector-ref xl i)) r))
                          (reverse r)))))
                 (else
                  (let ((n (len xl)))
                    (let lp ((i x) (r '()))
                      (if (< i n)
                          (lp (+ i 1) (cons (vector-ref xl i) r))
                          (reverse r)))))))
        (if (and (fluid? xl) (fstream-rw? (fluid-ref xl)))
            (format #t "endline (~a) : ~a~%" n
                    (list->string (reverse (freadline-l xl))))))))

(define file-next-line
  (case-lambda 
    ((data i)
     (error "not supported file-next-line verison"))
    ((data)
     (when (not (raws? data))      
       (freadline data)))))

(define (file-skip n data)
  (when (not (raws? data))      
    ((caddr data) (+ n (car data)))))

(define-parser-tool (<p-lambda> (X XL N M)) <p-define> .. xx <p-cc>)

(make-scanner-tools <p-lambda> <fail> <p-cc> <succeds> .. 
		    (X XL N M)
		    (c)
		    (d)
		    s-false s-true s-mk-seq s-mk-and s-mk-or)


(define wrap-n
  (lambda (f)
    (lambda (s p cc x xl n m . u)
      (let* ((uu  (reverse u))
             (c   (car uu))
             (cc2 (lambda (s p x xl n m c)
                    (apply cc s p x xl n m 
                           (reverse (cons c (cdr uu)))))))
      (f s p cc2 x xl n m c)))))

(define (f-n s p cc x xl n m . u)
  (apply cc s p x xl n m (reverse (cons n (cdr (reverse u))))))

(define (f-m s p cc x xl n m . u)
  (apply cc s p x xl n m (reverse (cons m (cdr (reverse u))))))

(define f-nl
  (let ((i (char->integer #\newline)))
    (lambda (s p cc x xxl n m . u)
      (if (raws? xxl)
          (if (feof2? x xxl)
              (p)
              (cond
               ((bytevector? xxl)
                (if (= (bytevector-ref xxl x) i)
                    (apply cc s p (+ x (Di)) xxl 0 (+ m 1) u)
                    (p)))
               (else
                (if (equal? #\newline (vector-ref xxl x))
                    (apply cc s p (+ x (Di)) xxl 0 (+ m 1) u)
                    (p)))))
          (match x
            ('()
             (call-with-values (lambda () (freadline xxl))
               (lambda (xl x)
                 (if (not (and (null? x) (feof? xxl)))
                     (apply cc s p x xl 0 (+ m 1) u)
                     (p)))))
            (_ (p)))))))

(define f-nl!
  (let ((i (char->integer #\newline)))
    (lambda (s p cc x xxl n m . u)
      (if (raws? xxl)
          (if (feof2? x xxl)
              (p)
              (cond
               ((bytevector? xxl)
                (if (= i (bytevector-ref xxl x))
                    (let* ((r (reverse u))
                           (q (reverse (cdr r)))
                           (c (car r))
                           (v (cons #\newline c))
                           (w (append q (list v))))
                      (apply cc s p (+ x (Di)) xxl 0 (+ m 1) w))
                    (p)))
               (else
                (if (equal? (vector-ref xxl x) #\newline)
                    (let* ((r (reverse u))
                           (q (reverse (cdr r)))
                           (c (car r))
                           (v (cons #\newline c))
                           (w (append q (list v))))
                      (apply cc s p (+ x (Di)) xxl 0 (+ m 1) w))
                    (p)))))
	    
          (match x
            ('()
             (call-with-values (lambda () (freadline xxl))
               (lambda (xl x)
                 (if (not (and (null? x) (feof? xxl)))
                     (let* ((r (reverse u))
                            (q (reverse (cdr r)))
                            (c (car r))
                            (v (cons #\newline c))
                            (w (append q (list v))))
                       (apply cc s p x xl 0 (+ m 1) w))
                     (p)))))
            (_ (p)))))))

  (define f-nl-pr
    (let ((i (char->integer #\newline)))
      (lambda (s p cc x xxl n m . u)
        (if (raws? xxl)
            (if (feof2? x xxl)
                (p)
                (cond
                 ((bytevector? xxl)
                  (if (= (bytevector-ref xxl x) i)
                      (begin
                        (format #t "~%")
                        (apply cc s p (+ x (Di)) xxl 0 (+ m 1) u))
                      (p)))
                 (else
                  (if (equal? #\newline (vector-ref xxl x))
                      (begin
                        (format #t "~%")
                        (apply cc s p (+ x (Di)) xxl 0 (+ m 1) u))
                      (p)))))
                    
            
            (match x
              ('()
               (call-with-values (lambda () (freadline xxl))
                 (lambda (xl x)
                   (if (not (and (null? x) (feof? xxl)))
                       (begin
                         (format #t "~%")
                         (apply cc s p x xl 0 (+ m 1) u))
                       (p)))))
              (_ (p)))))))

(define (f-retfkn lam)
  (wrap-n
   (lambda (s p cc x xl n m c)     
     (values
      (lam c)
      (lambda* (#:optional (c '()))
        (cc s p x xl n m c))))))

(define f-rev
  (wrap-n
   (lambda (s p cc x xl n m c)
     (if (raws? xl)
         (begin
           (fluid-set! rawdir (- (fluid-ref rawdir)))
           (cc s p x xl 0 0 c))
         (let* ((xxl (fstream-reverse xl)))
           (call-with-values (lambda () (freadline xxl))
             (lambda (xxl xx)
               (let lp ((n (length x)) (xx xx))
                 (if (> n 0)
                     (lp (- n 1) (cdr xx))
                     (cc s p xx xxl  0 0 c))))))))))
	       
(define f-do-nl
  (lambda (s p cc x xl n m . u)
    (if (raws? xl)
        (apply cc s p x xl 0 (+ m 1) u)
        (call-with-values (lambda () (freadline xl))
          (lambda (xl x)
            (if (not (feof? xl))
                (apply cc s p x xl 0 (+ m 1) u)
                (p)))))))

(define (s-scan-item nn k)
  (let ((i k))
    (lambda (f)
      (lambda (s p cc x xl n m . u)
	 (if (= i 0)
	     (begin
	       (set! i k)
	       (let ((xl-save xl))
		 (apply f s p (lambda (s p x xl n m . u)
                                (file-skip nn xl-save)
                                (apply cc s p x xl n m u))
		    x xl n m u)))
	     (begin
	       (set! i (- i 1))
	       (apply f s p cc x xl n m u)))))))


(define allchars! (Ds (f-tag! ".")))

(define (f-seek0 n i)
  (<p-lambda> (c)
    (if (< i n)
        (let ()
          (.. (c2) ((f-or! allchars! f-nl) c))
          (.. ((f-seek0 n (+ i 1)) c)))
        (<p-cc> c))))


(define-syntax-rule (setup-parser-0
		     <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx
		     X XL ((N NI) (M MI) (XX Init) ...)
		     s-false s-true s-mk-seq s-mk-and s-mk-or
                     f-read f-test f-test! f-test2 f-test2!
                     pr-test f-tag f-tag! pr-tag f-let
                     chtr f-id f-pk f-pkk s-tag s-tag! spr-tag
		     s-seq s-and s-and! s-and!! s-and-i s-or s-or-i
                     f-or f-or! f-and f-and! f-and!! f-seq
		     f? f+ f-nm f* ff? ff+ ff*
                     ng* ng+ ng? g* g+ g? gg* gg+ gg? gmn ngmn
                     f-line f-cons f-list f-cons* f-append mk-token mk-token-raw
                     p-freeze
		     parse parse-raw
                     f-out f-skip
                     f-true f-false ss f< f> fn f-eof
                     mk-simple-token f-clear-body
                     f-char f-char! pr-char f-reg f-reg! pr-reg
                     f-ws f-ws* pr-ws+ tok-ws* tok-ws+ parse-no-clear
                     s-rpl f-rpl s-tr f-tr f-1-char f-1-char! pr-1-char
                     f-not f-not! f-not* pr-not f-seq! f-seq!! f-deb
		     pp do-print f-wrap f-tag-pr f-ichar
                     f-if f-if! f-cond f-cond! f-check f-checkr D f-addchar
                     f-collect f-scope f-prev f-ftr f-pos f-seek
                     f-maxlen f-stepped)
(begin

  (define (f-skip n m)
    (<p-lambda> (c)
      (if (and (= n N) (= m M))
	  (<p-cc> c)
	  (<and>
	   (.. (c2) (allchars! c))
	   (.. ((f-skip n m) c2))))))

(define (D f)
  (<p-lambda> (c)
     (.. (f c))))

(define (f-maxlen n)
  (<p-lambda> (c)
    (<code> (fluid-set! maxlen n))
    (<p-cc> c)))

(define (f-seek n) (f-seek0 n 0))

(define (h x2 n m)
  (lambda xx (apply (f-skip n m) x2)))

(define (g f x3)
  (<p-lambda> (c)
     (.. (c2) (f c))
     (let ((n N) (m M))
       (.. ((h x3 n m) c2)))))

(define (f-pos f)
  (<p-lambda> (c)
     (if (raws? XL)
         (<code> (f X M N))
         (<code> (f (- (fposition XL) (length X)
                       (let ((x (struct-ref XL 2)))
                         (if (and (pair? x) (eq? (car x) #\newline))
                             1
                             0)))                                       
                    M N)))
     (<p-cc> c)))

(define f-stepped
  (<p-lambda> (c)
    (let ((pos (list N M)))
      (if (equal? (fluid-ref oldpos) pos)
          <fail>
          (<and>
           (<code> (fluid-set! oldpos pos))
           (<p-cc> c))))))
              
(define (f-pos f)
  (<p-lambda> (c)
     (if (raws? XL)
         (<code> (f X M N))
         (<code> (f (- (fposition XL) (length X)
                       (let ((x (struct-ref XL 2)))
                         (if (and (pair? x) (eq? (car x) #\newline))
                             1
                             0)))                                       
                    M N)))
     (<p-cc> c)))

(define (f-scope f)
  (lambda x (apply (g f x) x)))
            

(define f-read
 (<p-lambda> (c)
   (when (next-char? X XL)
     (let ((a (chtr (first X XL)))
	   (l (next X XL))
	   (n (+ N 1)))
       (<syntax-parameterize> ((X (lambda x #'l))
			       (N (lambda x #'n)))
	 (<p-cc> a))))))


(define (f-checkr f)
  (<p-lambda> (c)    
    (when (f c)
      (<p-cc> c))))

(define (f-check f) (f-checkr (lambda (c) (f (if (pair? c) (car c) c)))))

(define (f-ftr f)
  (<p-lambda> (c)    
    (<p-cc> (f c))))

			  
(define (f-test f)
 (<p-lambda> (c)
   (when (next-char? X XL)
     (let* ((a  (chtr (btr (first X XL))))
	    (l  (next X XL))
	    (n  (+ N 1)))
       (<syntax-parameterize> ((X (lambda x #'l))
			       (N (lambda x #'n)))
        (when (f a)
          (<p-cc> c)))))))

(define (f-test2 f)
 (<p-lambda> (c)
   (when (next-char? X XL)
     (let* ((a  (first X XL))
	    (l  (next X XL))
	    (n  (+ N 1)))
       (<syntax-parameterize> ((X (lambda x #'l))
			       (N (lambda x #'n)))
        (when (f a)
          (<p-cc> c)))))))

(define (f-test! f)
 (<p-lambda> (c)
   (when (next-char? X XL)
     (let* ((a  (chtr (btr (first X XL))))
	    (l  (next X XL))
	    (n  (+ N 1)))
       (if (f a)
	   (let ((cc (cons a c)))
	     (<syntax-parameterize> ((X (lambda x #'l))
				     (N (lambda x #'n)))
               (<p-cc> cc))))))))

(define (f-test2! f)
 (<p-lambda> (c)
   (when (next-char? X XL)
     (let* ((a  (first X XL))
	    (l  (next X XL))
	    (n  (+ N 1)))
       (if (f a)
	   (let ((cc (cons a c)))
	     (<syntax-parameterize> ((X (lambda x #'l))
				     (N (lambda x #'n)))
               (<p-cc> cc))))))))


(define (pr-test f)
   (<p-lambda> (c)
   (when (next-char? X XL)
     (let* ((a  (chtr (first X XL)))
	    (l  (next X XL))
	    (n  (+ N 1)))
       (if (f a)
	   (<and>
	    (<format> #t "~a" a)
	    (<p-cc> c)))))))

(define (chtr x) ((fluid-ref *translator*) x))

(<p-define> (f-id c) (<p-cc> c))

(s-mk-seq s-seq   <and>)

(s-mk-and s-and   <and>)
(s-mk-and s-and!  <and!>)
(s-mk-and s-and!! <and!!>)

(s-mk-or  s-or    <or>)

(define (f-pk nm)
  (<p-lambda> (c)
    (<pp-dyn> `(,nm ,(head-n 10 X XL) ,c) `(leaving ,nm))
    (<p-cc> c)))

(define (f-pkk nm)
  (<p-lambda> (c)
    (<pp-dyn> `(,nm ,X ,XL ,c) `(leaving ,nm))
    (<p-cc> c)))

(define (f-deb nm)
  (<p-lambda> (c)
    (if do-print
        (<pp-dyn> `(,nm ,X ,c) `(leaving ,nm))
        <cc>)
    (<p-cc> c)))

(define (f-out tag)
  (<p-lambda> (c)
    (if (char? tag)
        (<p-cc> (cons tag c))
        (<p-cc> tag))))

(define (f-nm n m)
  (<p-lambda> (c)
     (if (and (if (number? n) (= n N) #t)
	      (if (number? m) (= m M) #t))
	 (<p-cc> c)
	 <fail>)))

(define (f-wrap s f)
  (case-lambda
    ((x . l)
     (if (symbol? x)
         (s-seq (f-deb `(,x ,s)) (apply f l) (f-deb `(success ,x ,s)))
         (apply f x l)))
    (()
     (f))))

(define (f-collect . l)
  (<p-lambda> (c)
    (let ((s S))
       (let lp ((l l) (r '()))
         (if (pair? l)
             (<with-s> s
               (<or>
                (<and>
                 (.. (x) ((car l) '()))
                 <cut>
                 (lp (cdr l) (cons x r)))
                (lp (cdr l) r)))
             (when (pair? r)
               (<p-cc> r)))))))

(define (f-addchar ch)  (<p-lambda> (c) (<p-cc> (cons ch c))))

(define (ss x)
  (cond
   ((string? x)
    (let ((ws (fluid-ref *whitespace*)))
      (f-seq ws (f-tag x) ws)))
   ((procedure?  x)
    x)
   (else
    (f-out x))))

(define f-and
  (f-wrap 'f-and
    (case-lambda 
      ((f)    (ss f))
      ((f g)  (s-and (ss f) (ss g)))
      ((f g . l) (s-and (ss f) (ss g) (apply f-and l)))
      (()     s-true))))

(define f-and!
  (f-wrap 'f-and!
    (lambda l
      (s-and! (apply f-and l)))))

(define f-or
  (f-wrap 'f-or
   (case-lambda 
     ((f)       (ss f))
     ((f g)     (s-or (ss f) (ss g)))
     ((f g . l) (s-or (ss f) (ss g) (apply f-or l)))
     (()         s-false))))

(define f-or!
  (f-wrap 'f-or!
   (lambda x
     (f-and! (apply f-or  x)))))
    
(define (f> f n)    (letrec ((ret (lambda (n)
				    (if (= n 0)
					s-true
					(s-seq f (ret (- n 1)))))))
		      (ret n)))
(define (f< f n)    (letrec ((ret (lambda (n)
				    (if (= n 0)
					s-true
					(f-or! (s-seq f (ret (- n 1)))
					       s-true)))))
		      (ret n)))
(define (fn f n m) (s-and! (s-seq (f> f n) (f< f (- m n)))))

(define (mk-simple-token nm f)
 (<p-lambda> (x)
     (.. (cout) (f '()))
     (<p-cc> `(nm ,(list->string cout)))))

(<p-define> (f-eof cin)
  (if (raws? XL)
      (if (feof2? X XL)
          (<p-cc> cin)
          <fail>)
      (if (and (null? X)
               (call-with-values (lambda () (freadline XL))
                 (lambda (xl x) (feof? xl))))
          (<p-cc> cin)
          <fail>)))

(define (f-clear-body f)
  (<p-lambda> (c)
    (let ((op P)
	  (os S)
	  (p  S))
      (<with-s> p
        (.. (c) (f c))
	(<with-fail> op
          (<with-s> os
            (<p-cc> c)))))))

(define (f-char!  ch) (f-test! (lambda (x) (eq? x ch))))   
(define (f-char   ch) (f-test  (lambda (x) (eq? x ch))))   
(define (pr-char  ch) (pr-test (lambda (x) (eq? x ch))))   


(define (char->string x) (list->string (list x)))
(define (f-reg pat-str)
  (let ((reg (make-regexp pat-str)))
    (f-test (lambda (x) 
	      (let ((x (char->string x)))
		(regexp-exec reg x))))))

(define (f-reg! pat-str)
  (let ((reg (make-regexp pat-str)))
    (f-test! (lambda (x) 
	       (let ((x (char->string x)))
		 (regexp-exec reg x))))))

(define (pr-reg pat-str)
  (let ((reg (make-regexp pat-str)))
    (pr-test (lambda (x) 
	       (let ((x (char->string x)))
		 (regexp-exec reg x))))))

;; The idea of this function is to perform a tokenizing activity
;; utilizing this means that we loose the ability to redo and undo
;; inside the scanner part.
;; This is not ass effective as a regexp tokenizer but should be a
;; much faster then doing a full parser of everything.

(define mk-token
  (case-lambda
    ((f)
     (<p-lambda> (c)
        (.. (cc) (f '()))	       
        (<p-cc> (list->string (reverse cc)))))
    ((f lam)
     (<p-lambda> (c)
        (.. (cc) (f '()))	       
        (<p-cc> (lam (list->string (reverse cc))))))))

(define mk-token-raw
  (case-lambda
    ((f lam)
     (<p-lambda> (c)
        (.. (cc) (f '()))	       
        (<p-cc> (lam cc))))))


(define (p-freeze tok f mk)
  (<p-lambda> (c)
     (<and!>
      (let ((val 
	     (vhash-ref (fluid-ref *freeze-map*)
			(cons* N M tok) #f))
	    (op  P)
	    (os  S))
	(if (not val)
	    (let ((n N) (m M))
	       (<or>
                (<and>
                 (.. (cc) (f c))
                 (let ((val2 (mk S c cc)))		   
              	   (<with-fail> op
		   (<with-s>    os
                    (<code>
                     (fluid-set!
                      *freeze-map*                
                      (vhash-cons 
                       (cons* n m tok) 
                       (list X XL N M XX ... val2)
                       (fluid-ref *freeze-map*))))
                    (<p-cc> val2)))))
                (let ((val2 'fail))
                  (<code>
                   (fluid-set!
                    *freeze-map*                
                    (vhash-cons 		   
                     (cons* n m tok) val2
                     (fluid-ref *freeze-map*))))
                  <fail>)))
	     (if (pair? val)
		 (<and>
		  (<with-fail> op
		  (<with-s>    os
		   (<apply> f-true val))))
		 <fail>))))))

(define (f* f) (letrec ((ret (f-or (s-seq f ret) s-true))) (s-and! ret)))
(define (f+ f) (s-seq f (f* f)))
(define f-ws  (f-or! (f-char #\space) f-nl (f-char #\tab)))
(define f-ws* (f* f-ws))
(define f-ws+ (f+ f-ws))

(define tok-ws* (f-or!
		 (p-freeze 'ws* (mk-token (s-seq f-ws f-ws f-ws*))
			   (lambda (s cin cout) cin))
		 f-ws*))

(define tok-ws+ (f-or!
		 (p-freeze 'ws+ (mk-token (s-seq f-ws f-ws f-ws*))
			   (lambda (s cin cout) cin))
		 f-ws+))

(define parse*
  (case-lambda
   ((str matcher)
    (define f
      (lambda (stream)
        (with-fluids ((*freeze-map* vlist-null)
                      (maxlen       -1))
          (clear-tokens)
          ((<lambda> ()
            (<values> (x xl n . l)
             (matcher (start-x stream) (make-file-reader stream) 
                      NI (start-m stream MI) Init ... '()))
            (<code> (print-last-line x n (if xl xl '())))
            (<cc> (car (reverse l))))
           '()
           (lambda () #f)
           (lambda (s p cc) cc)))))
    (if (string? str)
	(f (make-fluid (rw-fstream-from-string str)))
	(f str)))

   ((matcher)
    (with-fluids ((*freeze-map* (fluid-ref *freeze-map*))
                  (maxlen       -1))
      (clear-tokens)
      ((<lambda> ()
	(<and>
	 (<values> (x xl n . l)
		   (matcher '() (make-file-reader) NI MI Init ... '()))
	 (<code> (print-last-line x n (if xl xl '())))
	 (<cc> (car (reverse l)))))
       '()
       (lambda () #f)
       (lambda (s p cc) cc))))))

(define parse
  (case-lambda
    ((str matcher)
     (parse* str
             (if (or (and (string? str) (= (string-length str) 0))
                     (raws? str))
                 matcher
                 (f-seq f-nl matcher))))
    ((matcher)
     (parse* matcher))))

(define parse-raw
  (case-lambda
    ((str matcher)
     (parse* str matcher))
    ((matcher)
     (parse* matcher))))

(define (equalize stream x xl nstart)
  (define n (- (rw-fstream-n xl) (length x)))
  (if (port? stream)
      (seek stream (+ nstart n) SEEK_SET)
      (let ((s (fluid-ref stream)))
	(fluid-set! stream (fmove s n)))))

(define (get-pos stream)
  (cond
   ((port? stream)
    (file-position stream))
   ((fluid? stream)
    (let ((s (fluid-ref stream)))
      (if (fstream? s)
	  (fposition s)
	  0)))
   (else
    0)))

(define parse-no-clear
  (case-lambda
   ((stream str matcher)
    (define N (get-pos stream))
    (define f
      (lambda ()
	(with-fluids ((*freeze-map* (fluid-ref *freeze-map*)))
	  (clear-tokens)
	  (<run> 1 (cout) 
	     (<values> (x xl n m out)
	       (matcher (start-x stream) (make-file-reader stream) 0 0 '()))
	     (<code> 
	      (equalize stream x xl N)
	      (print-last-line x n xl))
	     (<=> cout out)))))

    (if (string? str)
	(with-input-from-string str f)
	(with-input-from-port   str f)))

   ((stream matcher)
    (define N (get-pos stream))
    (with-fluids ((*freeze-map* (fluid-ref *freeze-map*)))
      (clear-tokens)
      (<run> 1 (cout) 
	   (<values> (x xl n m out)
		     (matcher (start-x stream)
                              (make-file-reader stream) 0 0 '()))
           (<code>
	    (equalize stream x xl N) 
	    (print-last-line x n xl))
	   (<=> cout out))))))


(define (s-rpl m y)
  (<p-lambda> (c)
    (.. (c) (m c))
    (<format> #t "~a" y)
    (<p-cc> c)))

(define (f-rpl m f)
  (<p-lambda> (c1)
    (.. (c2) (m c1))
    (<code> (f c2))
    (<p-cc> c2)))


(define-syntax-rule (s-tr x y) (s-rpl (s-tag x) y))
(define-syntax-rule (f-tr x y) (f-rpl (mk-token (s-tag! x)) y))
	    
(define f-1-char  (f-reg  "."))
(define f-1-char! (f-reg! "."))
(define pr-1-char (pr-reg "."))

(define (f-not f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (.. (c) (f-1-char c))
    (<p-cc> c)))

(define (f-not* f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (<p-cc> c)))

(define (f-not! f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (.. (c) (f-1-char! c))
    (<p-cc> c)))

(define (f-ichar ch)
  (let ((ch (if (string? ch) 
		(car (string->list ch))
		ch)))
    (<p-lambda> (c)
       (<p-cc> (cons ch c)))))

(define (pr-not f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (.. (c) (pr-1-char c))
    (<p-cc> c)))

(define f-seq
  (f-wrap 'f-seq
    (case-lambda 
      ((f)       (ss f))
      ((f g)     (s-seq (ss f) (ss g)))
      ((f g . l) (s-seq (ss f) (ss g) (apply f-seq l)))
      (()         s-true))))

(define f-line
  (<p-lambda> (c)
   (<p-cc> (cons M N))))

(define (f-if  p x z) (f-or  (f-and p x) z))
(define (f-if! p x z) (f-or! (f-and p x) z))

(define f-cond
  (case-lambda
   (()  f-false)
   ((x) x)
   ((p  x)
    (f-and p x))
   ((p x . l)
    (f-if p x (apply f-cond l)))))

(define f-cond!
  (case-lambda
   (()  f-false)
   ((x) x)
   ((p x)
    (f-and p x))
   ((p x . l)
    (f-if! p x (apply f-cond l)))))

(define-syntax f-let
  (lambda (x)
    (syntax-case x ()
      ((_ ((v f) (... ...)) code (... ...))
       (with-syntax (((p (... ...)) (map (lambda x #f) #'(v (... ...)))))
	 #'(<p-lambda> (c)
	     (.. (v) (f p))
             (... ...)          
             (.. ((f-seq code (... ...)) c))))))))

(define none (list 'none))
(define (pos line1 col1 line2 col2 x)
  (set-object-property! x 'position (list line1 col1 line2 col2))
  x)
  
(define f-cons
  (f-wrap 'f-cons
   (lambda (f g)
     (<p-lambda> (c)
      (let ((n1 N) (m1 M))
        (.. (c1) ((ss f) none))
        (.. (c2) ((ss g) c1))
        (let ((n2 N) (m2 M))
          (if (eq? c1 none)
              (<p-cc> (pos m1 n1 m2 n2 c2))
              (<p-cc> (pos m1 n1 m2 n2 (cons c1 c2))))))))))

(define f-cons*
  (f-wrap 'f-cons*
    (case-lambda
      ((x)    (ss x))
      ((x y)  (f-cons x y))
      ((x y . l)
       (f-cons x (apply f-cons* y l))))))

(define f-list
  (f-wrap 'f-list
    (case-lambda
      ((x)    (f-cons x (f-out '())))
      ((x y)  (f-cons x (f-cons y (f-out '()))))
      ((x y . l)
       (f-cons x (apply f-list y l))))))

(define f-append
  (f-wrap 'f-list
    (case-lambda
      ((x)    x)
      ((f g)  
       (<p-lambda> (c)
         (.. (c1) ((ss f) c))
         (.. (c2) ((ss g) c1))
         (<p-cc> (append c1 c2))))
       
      ((x y . l)
       (f-append x (apply f-append y l))))))


(define f*
  (f-wrap 'f*
    (lambda (x)
      (f-or! (s-seq (s-and! (ss x)) (Ds (f* x))) s-true))))

(define g*
  (f-wrap 'f*
    (lambda (x)
      (f-or  (s-seq (ss x) (Ds (g* x))) s-true))))

(define ng*
  (f-wrap 'ng*
    (lambda (x)
      (f-or s-true (s-seq (ss x) (Ds (ng* x)))))))

(define ff*
  (f-wrap 'ff*
   (case-lambda 
    ((x)
     (f-or! (f-cons (f-and! (ss x)) (Ds (ff* x))) (f-out '())))
    ((x d)
     (f-or! (f-cons (f-and! (ss x)) (Ds (ff* x d))) (f-out d))))))

(define gg*
  (f-wrap 'gg*
   (case-lambda 
     ((x)
      (f-or (f-cons (ss x) (Ds (gg* x))) (f-out '())))
     ((x d)
      (f-or (f-cons (ss x) (Ds (gg* x d))) (f-out d))))))

(define f?
  (f-wrap 'f?
    (lambda (x)
      (f-or! (ss x) s-true))))

(define g?
  (f-wrap 'g?
    (lambda (x)
      (f-or (ss x) s-true))))

(define ng?
  (f-wrap 'g?
    (lambda (x)
      (f-or s-true (ss x)))))

(define ff? 
  (f-wrap 'ff?
    (case-lambda
      ((x)
       (f-or! (ss x) (f-out #f)))
      ((x default)
       (f-or! (ss x) (f-out default))))))

(define gg? 
  (f-wrap 'gg?
    (case-lambda
      ((x)
       (f-or (ss x) (f-out #f)))
      ((x default)
       (f-or (ss x) (f-out default))))))
   

(define f+ 
  (f-wrap 'f+
    (lambda (x)
      (s-seq (ss x) (f* x)))))

(define g+ 
  (f-wrap 'g+
    (lambda (x)
      (s-seq (ss x) (g* x)))))

(define ng+ 
  (f-wrap 'ng+
    (lambda (x)
      (s-seq (ss x) (ng* x)))))

(define ff+
  (f-wrap 'ff+
    (lambda (x . l)
      (f-cons (ss x) (apply ff* x l)))))

(define gg+
  (f-wrap 'gg+
    (lambda (x . l)
      (f-cons (ss x) (apply gg* x l)))))

(define fmn
  (f-wrap 'fnm
     (lambda (f m n)     
       (let lp ((i 0))
         (if (< i m)
             (f-seq f (lp (+ i 1)))
             (let lp ((i i))
               (if (< i n)
                   (f-or! (f-seq f (lp (+ i 1)))
                          f-true)
                   f-true)))))))

(define gmn
  (f-wrap 'gnm
     (lambda (f m n)     
       (let lp ((i 0))
         (if (< i m)
             (f-seq f (lp (+ i 1)))
             (let lp ((i i))
               (if (< i n)
                   (f-or (f-seq f (lp (+ i 1)))
			 f-true)
		   f-true)))))))

(define ngmn
  (f-wrap 'ngnm
     (lambda (f m n)     
       (let lp ((i 0))
         (if (< i m)
             (f-seq f (lp (+ i 1)))
             (let lp ((i i))
               (if (< i n)
                   (f-or f-true (f-seq f (lp (+ i 1))))
		   f-true)))))))
                      
(define-syntax s-tag
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (let* ((w (syntax->datum #'x))
	      (w (cond
		  ((symbol? w) (string->list (symbol->string w)))
		  ((string? w) (string->list w))
		  (else #f))))
	 (if w		   		 
	     (with-syntax ((chs w))
               #'(apply f-seq (map f-char chs)))
	     (error "argument to s-tag is either string or symbol")))))))

(define-syntax s-tag!
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (let* ((w (syntax->datum #'x))
	      (w (cond
		  ((symbol? w) (string->list (symbol->string w)))
		  ((string? w) (string->list w))
		  (else #f))))
	 (if w		   		 
	     (with-syntax ((chs w))
		#'(apply f-seq (map f-char! chs)))
	     (error "argument to s-tag is either string or symbol")))))))

(define-syntax spr-tag
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (let* ((w (syntax->datum #'x))
	      (w (cond
		  ((symbol? w) (string->list (symbol->string w)))
		  ((string? w) (string->list w))
		  (else #f))))
	 (if w		   		 
	     (with-syntax ((chr w))
		#'(apply f-seq (map pt-char (list . chr))))
	     (error "argument to s-tag is either string or symbol")))))))

(define (f-tag x)
  (let ((l ((@ (guile) map) (lambda (x) (f-char x))
	    (string->list (format #f "~a" x)))))
    (apply f-seq l)))
(define (f-tag! x)
  (let ((l ((@ (guile) map) (lambda (x) (f-char! x))
	    (string->list (format #f "~a" x)))))
    (apply f-seq l)))
(define (f-tag-pr x)
  (let ((l ((@ (guile) map) (lambda (x) (pr-char x))
	    (string->list (format #f "~a" x)))))
    (apply f-seq l)))

(define (f-seq!  . x) (f-and! (apply f-seq x)))
(define (f-seq!! . x) (apply f-seq (map (lambda (x) (f-and! x)) x)))
(define (f-and!! . x) (apply f-and (map (lambda (x) (f-and! x)) x)))

(define f-true s-true)
(define f-false s-false)

(define (f-prev n f)
  (f-seq f-rev
	 (let lp ((i 0))
	   (if (< i n)
	       (f-seq (f-or f-nl (f-test (lambda (x) #t)))
		      (lp (+ i 1)))
	       (f-seq f-rev f)))))

))

(eval-when (compile load eval)
           (define names '(f-read f-test f-test! f-test2 f-test2!
                                  pr-test f-tag f-tag! pr-tag f-let
                                  chtr f-id f-pk f-pkk s-tag s-tag! spr-tag
                                  s-seq s-and s-and! s-and!! s-and-i s-or s-or-i
                                  f-or f-or! f-and f-and! f-and!!
                                  f-seq f? f+ f-nm f* ff? ff+ ff*
                                  ng* ng+ ng? g* g+ g? gg* gg+ gg? gmn ngmn
                                  f-line f-cons f-list f-cons* f-append
                                  mk-token mk-token-raw p-freeze parse
                                  parse-raw f-out f-skip
                                  f-true f-false ss f< f> fn f-eof
                                  mk-simple-token f-clear-body
                                  f-char f-char! pr-char f-reg f-reg! pr-reg
                                  f-ws f-ws* pr-ws+ tok-ws* tok-ws+
                                  parse-no-clear
                                  s-rpl f-rpl s-tr f-tr f-1-char f-1-char!
                                  pr-1-char
                                  f-not f-not! f-not*  pr-not f-seq! f-seq!!
                                  f-deb
                                  pp do-print f-wrap f-tag-pr
                                  f-ichar
                                  f-if f-if! f-cond f-cond! f-check f-checkr D
                                  f-addchar f-collect
                                  f-scope f-prev f-ftr f-pos f-seek
                                  f-maxlen f-stepped)))

(define-syntax setup-parser
  (lambda (xxx)
    (syntax-case xxx ()
      ((w <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx 
          X XL ((N NI) ...)
          s-false s-true s-mk-seq s-mk-and s-mk-or)
       
       (with-syntax (((nm ...) (map (lambda (x) 
                                      (datum->syntax #'w x))
                                    names)))
       #'(begin
	   (setup-parser-0
	    <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx 
	    X XL ((N NI) ...)
	    s-false s-true s-mk-seq s-mk-and s-mk-or
	    nm ...)

	   (export nm) ...))))))
	 

;; Creating the standard parser
(setup-parser  <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx 
               X XL ((N 0) (M 0))
               s-false s-true s-mk-seq s-mk-and s-mk-or)
