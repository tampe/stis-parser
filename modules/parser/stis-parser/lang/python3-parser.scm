;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (parser stis-parser lang python3-parser)
  #:use-module (parser stis-parser lang python3 formatter)
  #:use-module (parser stis-parser macros)
  #:use-module ((parser stis-parser) #:select (*whitespace* f-n f-m))
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (ice-9 unicode)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 pretty-print)
  #:use-module (parser stis-parser lang python3 tool)
  #:export (p pe python3-parser char-esc))

(set! (@@ (parser stis-parser lang python3 tool) do-print) #f)

(define in-subscript 1)

(define f-in-s
  (<p-lambda> (c)
    (<code> (set! in-subscript (- in-subscript 1)))
    (<p-cc> c)))

(define f-out-s
  (<p-lambda> (c)
    (<code> (set! in-subscript (+ in-subscript 1)))
    (<p-cc> c)))

(define out-sub
  (<p-lambda> (c)
   (when (eq? in-subscript 0)
     (<p-cc> c))))
  
(define (in-sub x)
  (f-or!
   (f-seq f-in-s x f-out-s)
   (f-seq f-out-s f-false)))

(define (in-subn x)
  (f-or!
   (f-seq f-out-s x f-in-s)
   (f-seq f-in-s f-false)))


(define (mk-id x c cc) cc)

(define col0
  (<p-lambda> (c)
    (when (= M 0)
      (<p-cc> c))))

(define do-print #t)
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (let ()
         (define port (open-file "/home/stis/src/python-on-guile/log.txt" "a"))
         (with-output-to-port port
           (lambda ()
             (pretty-print `(,s ,(syntax->datum x)))))
         (close port)))
     x)
    ((x)
     (when do-print
       (let ()
         (define port (open-file "/home/stis/src/python-on-guile/log.txt" "a"))
         (with-output-to-port port
           (lambda ()
             (pretty-print (syntax->datum x))))
         (close port)))
     x)))

(define ppp
  (case-lambda
    ((s x)
     (pretty-print `(,s ,(syntax->datum x)))
     x)
    ((x)
     (pretty-print (syntax->datum x))
     x)))

(define-syntax-rule (Ds f) (lambda x (apply f x)))
(define-syntax-rule (DDs op f ...) (op (lambda x (apply f x)) ...))

(define  divide truncate/)
;; +++++++++++++++++++++++++++++++++++++ SCANNER SUBSECTION
(define nl (f-or f-nl f-eof))

(define *nl* 0)
(define nl++
  (lambda (s p cc . u)
    (define pp (lambda () (set! *nl* (- *nl* 1)) (p)))
    (set! *nl* (+ *nl* 1))
    (apply cc s pp u)))

(define nl-
  (lambda (s p cc . u)
    (define pp (lambda () (set! *nl* (+ *nl* 1)) (p)))
    (set! *nl* (- *nl* 1))
    (apply cc s pp u)))




(define g-nl
  (<p-lambda> (c)
     (if (> *nl* 0)
	 (.. (f-nl c))
	 <fail>)))

(define w    (f-or! (f-reg "[\t\r ]") g-nl))
(define wq   (f-or! (f-reg "[\t\r ]")))

(define com0 (f-seq! (f* w) "#" (f* (f-not f-nl))))
(define com  (f-or!
              (f-seq! col0 (f* w) com0 (f? f-nl))
              com0))
(define ddd  (f-or!
	      (f-seq (f-tag "...") (f* wq) f-nl)
	      (f-seq (f-tag "\\")  (f* wq) f-nl)))

(define ws+   (f-seq (f+ (f-or! com w ddd))))
(define ws*   (f-seq (f* (f-or! com w ddd))))
(define ws    ws*)
(define wsnl  (f-seq (f* (f-seq ws* f-nl))))
(define wsnl+ (f-seq (f* (f-seq ws* f-nl))))
(define ww  (f-or! w com0 ddd))
(define nl+ (f-seq nl++ ws*))
(define nc  (f-and (f-or! f-nl f-eof (f-not (f-reg "[_a-zA-Z0-9]"))) f-true))

(define (wn_ n i)
  (<p-lambda> (c)
     (cond
       ((> i n) <fail>)
       ((= i n)
        (.. ((f-and (f-not w) f-true) c)))
       ((< i n)
        (<or>
         (<and!>
          (.. (c) ((f-tag " ") c))
          (.. ((wn_ n (+ i 1)) c)))
         (<and!>
          (.. (c) ((f-tag "\t") c))
          (.. ((wn_ n (divide (+ i 4) 4)) c)))
         (<and!>
          (.. (c) ((f-tag "\r") c))
          (.. ((wn_ n i) c))))))))

(define (wn+_ n i)
  (<p-lambda> (c)
    (<or>
     (<and!>
      (.. (c) ((f-tag " ") c))
      (.. ((wn+_ n (+ i 1)) c)))
     (<and!>
      (.. (c) ((f-tag "\t") c))
      (.. ((wn+_ n (divide (+ i 8) 8)) c)))
     (<and!>
      (.. (c) ((f-tag "\r") c))
      (.. ((wn+_ n i) c)))
     (<and!>
      (when (> i n))
      (<with-bind> ((INDENT (cons i INDENT)))
        (<p-cc> c))))))

(define wn+
  (<p-lambda> (c)
    (let ((n (car INDENT)))
      (.. ((wn+_ n 0) c)))))

(define wn
  (<p-lambda> (c)
    (let ((n (car INDENT)))
      (.. ((wn_ n 0) c)))))

(define indent= wn)
(define indent+ wn+)
(define indent-
  (<p-lambda> (c)              
    (<with-bind> ((INDENT (cdr INDENT)))
      (<p-cc> c))))


(define identifier__ 
  (let ()
    (define ih  (f-reg! "[a-zA-Z_]"))
    (define i.. (f-or! 'or ih (f-reg! "[0-9]")))
    (mk-token
     (f-seq ih (f* i..)))))

(define keyw (make-hash-table))
(for-each
 (lambda (x) (hash-set! keyw (symbol->string x) #t))
 '(False None True and as assert break class continue def
	 del elif else except finally for from global if import
	 in is lambda nonlocal not or pass raise return try
	 while with yield))

(define decimal (mk-token (f-seq (f-reg! "[0-9]") (f* (f-reg! "[0-9]")))))
(define oct     (mk-token
                 (f-seq (f-tag "0")
                        (f-reg "[oO]") (f+ (f-reg! "[0-7]")))))
(define hex     (mk-token
		 (f-seq (f-tag "0")
                        (f-reg "[xX]") (f+ (f-reg! "[0-9a-fA-F]")))))
(define bin     (mk-token 
		 (f-seq (f-tag "0")
                        (f-reg "[bB]") (f+ (f-reg! "[01]")))))

(define integer
  (<p-lambda> (c)
    (<and!>
     (<or>
      (<and>
       (.. (c) (oct c))
       (<p-cc> (string->number c 8)))
      (<and>
       (.. (c) (hex c))
       (<p-cc> (string->number c 16)))
      (<and>
       (.. (c) (bin c))
       (<p-cc> (string->number c 2)))
      (<and>
       (.. (c) (decimal c))
       (<p-cc> (string->number c 10)))))))

(define intpart    (f+ (f-reg! "[0-9]")))
(define fraction (f-seq (f-tag! ".") intpart))
(define exponent (f-seq (f-reg! "[eE]") (f? (f-reg! "[+-]")) intpart))
(define pointfloat (f-or! (f-seq (f? intpart) fraction) 
			  (f-seq intpart (f-tag! "."))))
(define exponentfloat (f-seq (f-or intpart pointfloat) exponent))

(define floatnumber (mk-token (f-or! exponentfloat pointfloat)))
(define float
  (<p-lambda> (c)
    (.. (c) (floatnumber c))
    (<p-cc> (string->number c))))
  
(define imagnumber (f-seq (f-or! floatnumber integer)
                          (f-reg "[jJ]")))
(define imag
  (<p-lambda> (c)
    (.. (c) (imagnumber c))
    (<p-cc> (make-rectangular
             0 (if (string? c) (string->number c) c)))))

(define number
  (p-freeze 'number
    (f-or! imag float integer)
    mk-id))

(define identifier_
  (let ()
    (define (__*__ i)
      (match (string->list i)
	((#\_ #\_ . l)
	 (match (reverse l)
	   ((#\_ #\_ . l) #t)
	   (_ #f)))
	(_ #f)))

    (define (__* i)
      (match (string->list i)
	((#\_ #\_ . l)
	 #t)
	(_ #f)))

    (define (_* i)
      (match (string->list i)
	((#\_  . l)
	 #t)
	(_ #f)))

    (<p-lambda> (c)
     (.. (i) (identifier__ c))
     (cond 
      ((__*__  i)
       (<p-cc> `(#:identifier ,i #:system)))
      ((__*    i)
       (<p-cc> `(#:identifier ,i #:private)))
      ((_*     i)
       (<p-cc> `(#:identifier ,i #:local)))
      ((eq? i '_)
       (<p-cc> #:_))
      ((hash-ref keyw i)
       (<p-cc> `(#:keyword ,i)))
      (else
       (<p-cc> `(#:identifier ,i)))))))
       
(define identifierq 
  (<p-lambda> (c)
     (.. (i) (identifier_ c))
     (if (not (eq? (car i) #:keyword))
         (<p-cc> i)
         <fail>)))

(define identifier (f-or! identifierq (f-list #:identifier (Ds scmsym))))


;;;; +++++++++++++++++++++++++++++++++++++++++++++++ STRING +++++++++++++++
(define string-prefix
  (f-or!
   (f-seq (f-reg "[fF]") (f-out 'f))
   (f-seq (f-reg "[uU]")
          (f-or! (f-seq (f-reg "[rR]") (f-out #t))
                 (f-out #f)))
   (f-seq (f-reg "[rR]")
          (f-or! (f-reg "[uU]") f-true)
          (f-out #t))
   (f-out #f)))

(define (short-string-char raw? ch)
  (if raw?
      (f-or! (f-seq (f-tag "\\")  (f-tag! ch))
             (f-seq (f-tag! "\\") (f-tag! "\\"))             
             (f-not! (f-or! f-nl (f-tag ch))))
      (f-not! (f-or! f-nl (f-tag ch)))))

(define (long-string-char raw? ch)
  (if raw?
      (f-or!
       (f-seq (f-tag! "\\") (f-tag! ch))
       (f-or! f-nl! (f-not! (f-tag ch))))
      (f-or! f-nl! (f-not! (f-or! (f-tag "\\") (f-tag ch))))))

(define string-esc  (Ds char-esc))

(define (short-string-item raw? s)
  (if raw?
      (short-string-char raw? s)
      (f-or! string-esc  (short-string-char raw? s))))

(define (short-string-item-ff s)
  (mk-token
   (f*
    (f-and (f-not (f-tag "{"))
           (f-or! string-esc (short-string-char #f s))))))



(define (short-string-item-f s)
  (f-cons
   (short-string-item-ff s)
   (f-or! (f-cons
           (Ds (f-formatter test))
           (Ds (short-string-item-f s)))
          (f-out '()))))

(define (long-string-item raw? s)
  (if raw?
      (long-string-char raw? s)
      (f-or! string-esc  (long-string-char raw? s))))

(define (long-string-item-ff s)
  (mk-token
   (f*
    (f-and (f-not (f-tag "{"))
           (f-or! string-esc (long-string-char #f s))))))

(define (long-string-item-f s)
  (f-cons
   (long-string-item-ff s)
   (ff* (f-seq (Ds (f-formatter test)) (long-string-item-ff s)))))

(define (long-string raw?)
  (mk-token
   (f-or
    (f-seq! (f-tag "'''")
            (f* (long-string-item raw? "'''"))
            (f-tag "'''"))
    (f-seq! (f-tag "\"\"\"")
            (f* (long-string-item raw? "\"\"\""))
            (f-tag "\"\"\"")))))

(define (long-string-f)
  (f-or
   (f-seq! (f-tag "'''")
           (long-string-item-f "'''")
           (f-tag "'''"))
   (f-seq! (f-tag "\"\"\"")
           (long-string-item-f "\"\"\"")
           (f-tag "\"\"\""))))

(define (short-string raw?)
  (mk-token
   (f-or
    (f-seq! (f-tag "'")
            (f* (short-string-item raw? "'" ))
            (f-tag "'"))
    (f-seq! (f-tag "\"")
            (f* (short-string-item raw? "\""))
            (f-tag "\"")))))

(define (short-string-f)
  (f-or
   (f-seq! (f-tag "'")
           (short-string-item-f "'" )
           (f-tag "'"))
   (f-seq! (f-tag "\"")
           (short-string-item-f "\"")
           (f-tag "\""))))

(define scmsym
  (mk-token
   (f-seq (f-tag "|") (f* (f-not! (f-tag "|"))) (f-tag "|"))))

(define string_ 
  (f-let ((raw? string-prefix))
    (if (eq? raw? 'f)
        (f-seq (f-or! (long-string-f) (short-string-f)))
        (p-freeze 'string-literal
                  (f-seq (f-or! (long-string raw?) (short-string raw?)))
                  mk-id))))

(define p2 (lambda (s p cc x xl n m . u) (pk n m) (apply cc s p x xl n m u)))
		    
(define string
  (<p-lambda> (c)
    (.. (l) ((f-or!
	      (f-seq (f-cons (f-seq string_ nl+) (ff+ (f-seq (f* ww) string_)))
		     nl-)
	      string_) c))
			       
    (<p-cc>
     (catch #t
       (lambda ()
         (if (pair? l) (if (> (length l) 1) (apply + l) (car l)) l))
       (lambda x l)))))
;; ++++++++++++++++++++++++++++++++++++++++++ BYTE ++++++++++++++++++++++++++

(define bytes-prefix  
  (f-or!
   (f-seq! (f-reg "[bB]")
           (f-or (f-seq (f-reg "[rR]") (f-out #t))
                 (f-out #f)))
   (f-seq! (f-reg "[rR]") (f-reg "[bB]") (f-out #t))))

(define byte-special
  (let ()
    (define fx (f-reg! "[0-9a-fA-F]"))
    (define fo  (f-reg! "[0-7]"))
    (define he (mk-token (f-seq fx fx)    (lambda (x) (string->number x 16))))
    (define oc (mk-token (f-seq fo fo fo) (lambda (x) (string->number x 8))))
    (f-or!
     (f-seq (f-tag "a") (f-out 7))
     (f-seq (f-tag "b") (f-out 8))
     (f-seq (f-tag "f") (f-out 12))
     (f-seq (f-tag "n") (f-out 10))
     (f-seq (f-tag "r") (f-out 13))
     (f-seq (f-tag "t") (f-out 9))
     (f-seq (f-tag "v") (f-out 11))
     (f-seq (f-tag "x") he)     
     oc)))

(define char-esc
  (let ()
    (define fi   (f-reg! "[0-9a-fA-F]"))
    (define he4  (mk-token (f-seq fi fi fi fi)
                           (lambda (x) (string->number x 16))))
    (define he8  (mk-token (f-seq fi fi fi fi fi fi fi fi)
                           (lambda (x) (string->number x 16))))
    (define name (f-seq (f-tag "{") (mk-token (f+ (f-not! (f-tag "}"))))
                        (f-tag "}")))
    (define (check n x)
      (if x x (error (format #f "Wrong unicode name N{~a}" n))))
    
    (f-seq (f-tag "\\")
           (f-or!
            f-nl
            (f-let ((x byte-special))
                   (f-out (integer->char x)))
            (f-seq (f-tag "N")
                   (f-let ((n name))
                     (f-out (check n (formal-name->char n)))))
            (f-seq (f-tag "u")
                   (f-let ((x he4))
                     (f-out (integer->char x))))
            (f-seq (f-tag "U")
                   (f-let ((x he8))
                     (f-out (integer->char x))))
            (f-reg! ".")))))

   
(define bytes-esc
  (let ()
    (define al (mk-token (f-reg! ".")
                         (lambda (s) (char->integer (string-ref s 0)))))
    (f-seq "\\"
           (f-or!
            byte-special
            al))))
                        
(define (short-bytes-char raw? nch)
  (mk-token (if raw?
                (f-not! (f-or! (f-tag nch) (f-tag "\n")))
                (f-not! (f-or! (f-tag nch) (f-reg "[\\\n]"))))
            (lambda (x) (char->integer (string-ref x 0)))))

(define (long-bytes-char raw? nch)
  (mk-token (if raw?
                (f-not! (f-tag nch))
                (f-not! (f-or! (f-tag nch) (f-tag "\\"))))
            (lambda (x) (char->integer (string-ref x 0)))))

(define (short-bytes-item raw? nch)
  (if raw?
      (short-bytes-char raw? nch)
      (f-or! (short-bytes-char raw? nch) bytes-esc)))

(define (long-bytes-item raw? nch)
  (if raw?
      (long-bytes-char raw? nch)
      (f-or! (long-bytes-char raw? nch) bytes-esc)))

(define (short-bytes raw?)
  (f-or! (f-seq! "'"  (ff* (short-bytes-item raw? "'"))  "'")
         (f-seq! "\"" (ff* (short-bytes-item raw? "\"")) "\"")))

(define (long-bytes raw?)
  (f-or! (f-seq! "'''"    (ff* (long-bytes-item raw? "'''"))    "'''")
         (f-seq! "\"\"\"" (ff* (long-bytes-item raw? "\"\"\"")) "\"\"\"")))

(define bytes_
  (p-freeze 'bytes-literal
    (f-let ((raw? bytes-prefix))
      (f-or! (long-bytes raw?)
             (short-bytes raw?)))
    mk-id))

(define bytes
  (<p-lambda> (c)
    (.. (l) ((f-or!
	      (f-seq (f-cons (f-seq bytes_ nl+) (ff+ (f-seq ws bytes_)))
		     nl-)
	      (f-list bytes_)) c))
			       
    (<p-cc> (if (= (length l) 1) (car l) (apply append l)))))

; +++++++++++++++++++++++++++++++++++ PARSER SUBSECTION +++++++++++++++++   
(define stmt            #f)
(define testlist        #f)   
(define dottaed_name     #f)
(define arglist         #f)
(define classdef        #f)
(define funcdef         #f)
(define test            #f)
(define small_stmt      #f)

(define match_stmt      #f)
(define expr_stmt       #f)
(define del_stmt        #f)
(define pass_stmt       #f)
(define flow_stmt       #f)
(define import_stmt     #f)
(define global_stmt     #f)
(define nonlocal_stmt   #f)
(define assert_stmt     #f)
(define testlist_star_expr   #f)
(define augassign       #f)
(define yield_expr      #f)
(define star_expr       #f)
(define exprlist        #f)
(define import_name     #f)
(define import_from     #f)
(define dotted_as_names #f)
(define import_as_names #f)
(define if_stmt         #f)
(define while_stmt      #f)
(define for_stmt        #f)
(define try_stmt        #f)
(define with_stmt       #f)
(define suite           #f)
(define except_clause   #f)
(define with_item       #f)
(define expr            #f)
(define or_test         #f)
(define lambdef         #f)
(define lambdef_nocond  #f)
(define and_test        #f)
(define not_test        #f)
(define comparison      #f)
(define comp_op         #f)
(define xor_expr        #f)
(define and_expr        #f)
(define or_expr         #f)
(define arith_expr      #f)
(define shift_expr      #f)
(define term            #f)
(define factor          #f)
(define power           #f)
(define atom            #f)
(define trailer         #f)
(define subscriptlist   #f)
(define testlist_comp   #f)
(define testlist_comp1  #f)
(define testlist_comp+  #f)
(define dictorsetmaker  #f)
(define comp_for        #f)
(define subscript       #f)
(define sliceop         #f)
(define argument        #f)
(define comp_if         #f)
(define yield_arg       #f)
(define dotted_name     #f)


(define file-input (f-seq (f* (f-or nl (f-seq indent= stmt))) (Ds wsnl) f-eof))
(define eval-input (f-seq testlist (f* nl) f-eof))
(define decorator (f-seq "@" (Ds test) ws f-nl))
(define decorators (ff+ decorator))

(define decorated (f-list #:decorated
			  (f-seq
                           decorators 
                           (f-and (f-or classdef funcdef) f-true))))

(define FALSE (f-out #f))
(define tfpdef
  (f-cons* #:tp identifier (f-or
                            (f-seq ":" ws (Ds test) ws)
                            FALSE)))

(define vfpdef identifier)
(define mk-py-list
  (lambda (targlist tfpdef)
    (let* ((arg=    (f-list #:=   tfpdef "=" (Ds test)))
	   (arg     (f-list #:arg tfpdef))
	   (arg1*   (f-list #:1*  "*"))
           (arg1/   (f-list #:1/  "/"))
	   (arg*    (f-list #:*   "*"  tfpdef))	   
	   (arg**   (f-list #:**  "**" tfpdef))
	   (f       (f-or! arg= arg* arg** arg arg1* arg1/)))
      (f-cons* (f-out targlist)  f (ff* (f-seq ws "," ws f))))))

(define e-it
  (lambda (msg)
    (<p-lambda> (c) (<code> (error (format #f "Syntax error: ~a at line ~a collumn ~a~%" msg M N))))))
(define e: (f-or! ":" (e-it "missing ':'")))

(define typedargslist (mk-py-list #:types-args-list tfpdef))
(define varargslist   (mk-py-list #:var-args-list   vfpdef))

(define parameters (f-seq! 'parameters
                           "(" nl+ (f-or typedargslist
					 (f-out '(#:types-args-list)))
			   ws nl- ")"))

(set! funcdef
  (p-freeze 'funcdef
    (f-list 'fundef
           #:def 
           (f-seq "def" identifier)
           parameters
           (ff? (f-seq! ws "->" ws (Ds test) ws))
           (f-seq ":" (Ds suite)))
    mk-id))

(define simple_stmt (f-list 'simple_stmt #:stmt
			    (f-seq 
			     (f-cons* #:comma (Ds small_stmt)
				     (ff* (f-seq ";" (Ds small_stmt))))
                             (f? ";") (f? ws) (f-or! nl f-eof))))

(define simple_stmt0 (f-list 'simple_stmt #:stmt
			    (f-seq 
			     (f-cons* #:comma (Ds small_stmt)
				     (ff* (f-seq ";" (Ds small_stmt))))
                             (f? ";") (f? ws) )))

(define type_expression
  (f-list
   #:type-exp
   (ff? (f-cons (Ds testlist_star_expr)
               (ff* (f-seq ws "," ws (Ds testlist_star_expr)))))
   (ff? (f-seq "*"  (Ds testlist_star_expr)))
   (ff? (f-seq "**" (Ds testlist_star_expr)))))

(set! small_stmt
  (Ds 
   (f-or 'small expr_stmt del_stmt pass_stmt flow_stmt import_stmt global_stmt
         nonlocal_stmt assert_stmt power)))

(set! expr_stmt 
 (f-list 'expr_stmt
   #:expr-stmt
   (Ds testlist_star_expr)
   (f-or!
    (f-list 'augassign #:augassign
	    (Ds augassign) ws
	    (f-or (Ds yield_expr) (Ds testlist)))
    
    (f-list 'assign #:assign-typed
            ws ":" ws type_expression
            ws "=" ws
            (Ds testlist_star_expr)
            (ff? (f-seq ws ":" ws type_expression)))
    
    (f-list 'assign #:assign-typed
            ws ":" ws type_expression)    
    (f-cons 'assign #:assign
	    (ff* (f-seq ws (f-tag "=") ws
			(f-or (Ds yield_expr)
			      (Ds testlist_star_expr))))))))

(set! testlist_star_expr
  (f-cons 'testlist_star_expr
   (f-or (Ds test) (Ds star_expr))
   (f-seq 
    (ff* (f-seq "," (f-or (Ds test) (Ds star_expr))))
    (f? ","))))


(set! augassign
  (mk-token
   (f-seq 'augassign
    ws
    (apply f-or!
	   (map f-tag!
		'("+=" "-=" "*=" "@=" "/=" "%=" "&=" "|=" "^="
		  "<<=" ">>=" "**=" "//=")))
    ws)))

(set! del_stmt  (f-cons 'del_stmt #:del (f-seq "del" (Ds exprlist))))

(set! pass_stmt (f-seq 'pass_stmt "pass" #:pass))
                    
(set! flow_stmt 
  (f-or 'flow_stmt
   (f-seq "break"    #:break)
   (f-seq "continue" #:continue)
   (f-list #:return "return" (ff? (Ds testlist)))
   (Ds yield_expr)
   (f-cons #:raise  (f-seq "raise" 
			   (f-or (f-cons*
                                  (Ds test)
                                  (ff? (f-seq "from" (Ds test)))
                                  (ff* (f-seq ws "," ws (Ds test))))
				 FALSE)))))

(set! import_name (f-cons (f-seq #:name "import") (Ds dotted_as_names)))
(set! import_stmt (f-list #:import
                          (f-or! 'import_stmt import_name (Ds import_from))))



(define dottir (mk-token (f-or! (f-tag! "...") (f-tag! "."))))
(define dots*  (ff* dottir))
(define dots+  (ff+ dottir))

(set! import_from
  (f-cons (f-seq 'import_from #:from "from")
          (f-cons
           (f-or (f-cons dots* (Ds dotted_name)) dots+)
           (f-seq ws (f-tag "import") ws
                  (f-or! (f-seq "*" FALSE)
                         (f-seq "(" nl+ (Ds import_as_names)
                                ws nl- ")")
                         (Ds import_as_names))))))

(define scm (f-list #:scm "scm" string))

(define import_as_name1
  (f-cons (f-seq identifier ws) (ff? (f-seq "as" identifier))))

(define import_as_name
  (f-cons import_as_name1
          (ff* (f-seq ws (f-tag ",") ws import_as_name1))))

(define dotted_as_name
  (f-cons (Ds dotted_name) (ff? (f-seq "as" identifier))))

(set! import_as_names
  (f-seq
   (f-cons import_as_name (ff* (f-seq "," import_as_name)))
   (f? ",")))
  
(set! dotted_as_names
  (f-cons dotted_as_name (ff* (f-seq "," dotted_as_name))))

(set! dotted_name
  (f-or!
   (f-cons* dots* identifier (ff* (f-seq "." identifier)))
   (f-cons  dots+ (f-out '()))))
  
(define comma_name
  (f-cons identifier (ff* (f-seq "," identifier))))

(set! global_stmt
  (f-cons 'global #:global (f-seq "global" comma_name)))

(set! nonlocal_stmt
  (f-cons 'nonlocal #:nonlocal (f-seq "nonlocal" comma_name)))

(set! assert_stmt
  (f-list 'assert #:assert
	  "assert"
          (f-cons (Ds test) (ff* (f-seq ws "," ws (Ds test))))
          (f-out
	   (catch #t
	     (lambda ()
	       (fluid-ref (@ (system base compile) %current-file%)))
	     (lambda x "<none>")))
          f-n
          f-m))


(define compound_stmt
  (Ds 
   (f-or! 'compound
          if_stmt
          while_stmt
          for_stmt
          try_stmt
          with_stmt
          funcdef
          classdef
          match_stmt
          decorated)))


(define single_input (f-or! (f-seq indent= simple_stmt) 
                            (f-seq indent= compound_stmt nl)
                            (f-seq (f-or nl f-eof))))

(set! stmt (f-or! 'stmt simple_stmt compound_stmt))

(define (ind f) (f-seq wsnl indent= (f-tag f) ws))

(define (walrus f)
  (f-or!
   (f-cons #:wal (f-list (f-seq ws identifier) (f-seq ws ":=" ws f)))
   f))


(set! if_stmt
  (f-cons 'if_stmt
   #:if
   (f-seq 
    "if"
    (f-cons (f-or! (walrus (in-sub (Ds test))) (e-it "wrong test expression"))
	    (f-seq e:  
		   (f-cons (f-or! (Ds suite) (e-it "wrong if block"))
			   (f-cons
			    (ff* (f-cons* (ind "elif")
					  (f-or! (in-sub (Ds test))
						 (e-it "wrong test expression")) 
					  e: 
					  (f-or! (Ds suite) (e-it "wrong bloc"))))
			    (ff? (f-seq (ind "else") e:
                                        (f-or! (Ds suite) (e-it "wrong blok")))))))))))

(set! while_stmt
  (f-cons 'while
   #:while
   (f-seq "while"
	  (f-or!
	   (f-cons (f-or! (walrus (in-sub (Ds test))) (e-it "wrong test expression"))
		   (f-seq e:
			  (f-cons (f-or! (Ds suite) (e-it "wrong while block"))
				  (ff? (f-seq (ind "else") e:
					      (f-or! (Ds suite)
						     (e-it "wrong else block")))))))
	   (e-it "wrong while statement")))))

(set! for_stmt
  (f-cons 'for
   #:for
   (f-seq "for"
	  (f-or!
	   (f-cons (Ds exprlist)
		   (f-seq (f-or! "in" (e-it "missing in in for statement"))
			  (f-or!
			   (f-cons (in-sub (Ds testlist))
				   (f-cons (f-seq e: (f-or! (Ds suite) (e-it "wrong for block")))
					   (ff? (f-seq (ind "else") e:
						  (f-or! (Ds suite) (e-it "wrong else block"))))))
			   (e-it "malformed in statement"))))
	   (e-it "malformed for statement")))))

(set! try_stmt
  (f-cons 'try
   #:try
   (f-seq ws "try" e:
	  (f-or!
	   (f-cons (Ds suite)
		   (f-or!
		    (f-cons
		     (ff* (f-list (Ds except_clause) e: (Ds suite))) 
		     (f-cons
		      (ff? (f-seq (ind "else") e:
				  (f-or! (Ds suite) (e-it "malformed else clause"))))
		      (ff? (f-seq (ind "finally") e:				 
				  ws
				  (f-or! (Ds suite) (e-it "malformed finally clause"))))))
		    (f-cons
		     FALSE
		     (f-cons
		      FALSE
		      (f-seq (ind "finally") e:
			     (f-or! (Ds suite) (e-it "malformed finally lock")))))))
	   (e-it "malformed try statement")))))

(set! with_item
  (f-or! (f-list (Ds test) "as" (f-or! (Ds expr) (e-it "missing second operator for 'as'")))	 
	 (f-list (Ds test))))
		     

(set! with_stmt
  (f-cons 'with
   #:with
   (f-or!
    (f-list "with" "(" 
            (f-cons
             (f-or! (f-seq ws with_item ws) (e-it "not a with item"))
             (ff* (f-seq ws* "," ws* (f-or! with_item (e-it "not a with item")) ws*)))
            ")" (f-seq e: (Ds suite)))

    (f-list "with"
            (f-cons
             (f-or! with_item (e-it "not a with item"))
             (ff* (f-seq ws* "," ws* (f-or! with_item (e-it "not a with item")) ws*)))
            (f-seq e: (Ds suite))))))


(define named_expression
  (f-or!
   (f-list #:named identifier ":=" (Ds test))
   (f-list #:expr  (Ds test))))

(define bitwise_or
  (f-cons
   (Ds test)
   (ff* (f-seq "|" (Ds test)))))

(define star_expression
  (f-or! (f-cons #:star bitwise_or)
         named_expression))

(define star_named_expression
  (f-or!
   (f-cons* #:bor "*" (Ds expr0))
   named_expression))

(define star_named_expressions
  (f-seq
   (f-cons star_named_expression
           (ff* (f-seq "," star_named_expression)))
   (f? ",")))

(define subject_expr
  (f-or!
   (f-cons
    #:star
    (f-cons star_named_expression
            (ff+ (f-seq "," star_named_expression))))
   named_expression))

(define literal_pattern
  (f-cons
   #:literal
   (f-or! (f-list #:add number "+" number)
          (f-list #:sub number "-" number)
          (f-cons #:number number)
          (f-cons #:string (ff+ string))
          (f-seq (f-tag "None")  #:None)
          (f-seq (f-tag "True")  #:True)
          (f-seq (f-tag "False") #:False))))

(define capture_pattern
  (f-cons
   #:capture
   (f-seq identifier (f-and (f-not (f-or! "." "(" "=")) f-true))))

(define wildcard_pattern
  (f-seq #:wildcard "_" (f-pk 'yes)))

(define name_or_attr
  (f-cons identifier (ff* (f-seq "." identifier))))

(define value_pattern
  (f-cons
   #:value
   (f-seq name_or_attr
          (f-and (f-not (f-or! "." "(" "=")) f-true))))

(define group_pattern
  (f-cons
   #:group
   (f-seq "(" (Ds pattern) ")")))

(define star_pattern
  (f-cons* #:star "*" (f-or! capture_pattern
                             wildcard_pattern)))

(define maybe_star_pattern
  (f-or!
   star_pattern
   (Ds pattern)))


(define sequence_pattern
  (f-cons
   #:seq
   (f-or! (f-seq "[" "]" (f-out '()))
          (f-seq "[" (f-cons maybe_star_pattern
                             (ff* (f-seq ","
                                         maybe_star_pattern)))
                 "]")
          (f-seq "(" ")" (f-out '()))
          (f-seq "(" (f-cons maybe_star_pattern
                             (ff* (f-seq ","
                                         maybe_star_pattern)))
                 (f? ",")
                 ")"))))

(define double_star_pattern
  (f-cons #:** (f-seq "**" capture_pattern)))

(define key_value_pattern
  (f-or!
   (f-list
    #:key
    (f-or! literal_pattern
           value_pattern)
    ":"
    (Ds pattern))
   double_star_pattern))

(define items_pattern
  (f-or!
   (f-cons key_value_pattern
           (ff* (f-seq "," key_value_pattern)))
   (f-out '())))

(define mapping_pattern
  (f-cons
   #:mapping
   (f-seq "{" items_pattern "}")))

(define positional_patterns
  (f-cons* #:pos (Ds pattern) (ff* (f-seq "," (Ds pattern)))))

(define keyword_pattern
  (f-list #:key identifier "=" (Ds pattern)))

(define keyword_patterns
  (f-cons* #:key keyword_pattern
           (ff* (f-seq "," keyword_pattern))))

(define class_pattern
  (f-cons
   #:class
   (f-or
    (f-list name_or_attr "(" ")" (f-out #f))
    (f-list name_or_attr "(" positional_patterns ")")
    (f-list name_or_attr "(" keyword_patterns ")")
    (f-list name_or_attr
            "(" positional_patterns ","
                keyword_patterns (f-or ",")
            ")"))))
    
(define closed_pattern
  (f-or! literal_pattern
         capture_pattern
         wildcard_pattern
         value_pattern
         group_pattern
         sequence_pattern
         mapping_pattern
         class_pattern))

(define or_pattern
  (f-cons #:or
          (f-cons closed_pattern
                  (ff* (f-seq "|" closed_pattern)))))

(define as_pattern
  (f-cons #:as (f-list or_pattern "as" capture_pattern)))

(define pattern
  (f-or! as_pattern or_pattern))

(define open_sequence_pattern
  (f-cons* #:seq maybe_star_pattern
           (ff+ (f-seq "," maybe_star_pattern))))

(define patterns
  (f-or! open_sequence_pattern pattern))

(define guard
  (f-seq "if" named_expression))

(define case_block
  (f-cons* "case" patterns
           (f-or! (f-list guard      (f-seq e: (Ds suite)))
                  (f-list (f-out #f) (f-seq e: (Ds suite))))))


(set! match_stmt
  (f-cons 'match
   #:match
   (f-seq
    (f-cons* "match" (Ds subject_expr) e: f-nl
             indent+ (ff* (Ds case_block)))
    indent-)))




(set! except_clause
  (f-seq 'except (ind "except")
	 (f-cons
          (ff? (f-or!
                (f-cons
                 (f-out #:list)               
                 (f-cons (f-seq "(" ws (Ds test) ws)
                         (f-seq (ff* (f-seq "," ws (Ds test) ws)) ")")))
                (Ds test)))
          (ff? (f-seq "as" (f-or! identifier (e-it "not an identifier")))))))

(set! suite  
  (f-cons #:suite
	  (f-or!
           (f-seq ws simple_stmt0 ws)
	   (f-seq wsnl+ indent+			
		  (f-cons
		   (f-or!
		    (f-seq! wsnl+ indent= (f-or! stmt atom))
		    stmt)
		   (ff* (f-seq wsnl indent= stmt)))
		  (f* (f-seq indent= ws f-nl))
		  indent-))))
	   

(set! test
  (f-or! 'test
   (f-list #:test
           (walrus (Ds or_test))
           (ff? (f-list 
                 (f-seq ws "if" ws (f-or! (Ds or_test) (e-it "malformed test expr")))
                 (ff? (f-seq ws "else" ws (f-or! (Ds test) (e-it "malformed test")))))))
   (Ds lambdef)))

(define test_nocond
  (f-or 'nocond (Ds or_test) (Ds lambdef_nocond)))

(set! lambdef
  (f-list 'lambdef
   #:lambdef
   (f-seq "lambda" (ff? (Ds varargslist) '()))
   (f-seq e: (Ds test))))

(set! lambdef_nocond
  (f-list 'lambdef_nocond
   'lambdef #:lambdef
   (f-seq "lambda" (ff? (Ds varargslist) '()))
   (f-seq e: test_nocond)))

(define (e-op x) (f-or! x (f-and (f-not (f-or (f-tag "/=")
					      (f-tag "<=")
					      (f-tag ">=")
					      (f-tag "=")))
				 (e-it "rhs of op is wrong"))))  
(set! or_test
  (p-freeze 'or_test
    (f-or! 'or_test
      (in-subn
       (f-cons #:or (f-cons (Ds and_test) (ff+ (f-seq ws (f-tag "or") nc ws
                                                      (e-op (Ds and_test)))))))
      (Ds and_test))
    mk-id))

(set! and_test
  (p-freeze 'and_test
    (f-or! 'and_test
      (in-subn              
       (f-cons #:and (f-cons (Ds not_test) (ff+ (f-seq ws (f-tag "and")
                                                       nc ws
                                                       (e-op (Ds not_test)))))))
      (Ds not_test))
    mk-id))

(set! not_test
  (f-or! 'not_test
     (in-subn
      (f-and
       (f-not (Ds comparison))
       (f-list #:not (f-seq ws (f-tag "not") nc ws (e-op (Ds not_test))))))
    (Ds comparison)))

(set! comparison
  (p-freeze 'comparison
    (f-or! 'comparison
      (in-subn               
       (f-cons #:comp 
               (f-cons (Ds expr)
                       (ff+ (f-cons (f-seq (Ds comp_op) ws) (e-op (Ds expr)))))))
      (Ds expr))
    mk-id))

(set! comp_op
  (f-or! 'comp_op
   (f-seq (f-seq ws (f-tag "not") ws (f-tag "in" ) nc ws ) (f-out "notin"))
   (f-seq (f-seq ws (f-tag "is" ) ws (f-tag "not") nc ws ) (f-out "isnot"))
   (apply f-or!
	  (map (lambda (x) (f-seq ws (mk-token (f-tag! x)) ws))
	       '("==" ">=" "<=" "<>" "!=" "in" "is" "<" ">" )))))
			  

(set! star_expr (f-cons 'star_expr #:starexpr (f-seq "*" (e-op (Ds expr)))))
(define (wwrap f) f)

(define expr0
  (wwrap
   (p-freeze 'expr
     (f-or! 'expr
       (in-subn
        (f-cons #:bor (f-cons (Ds xor_expr)
                              (ff+ (f-seq ws "|" ws (e-op (Ds expr)))))))
       (Ds xor_expr))
     mk-id)))

(set! expr (walrus (Ds expr0)))
  
(set! xor_expr
  (wwrap
   (p-freeze 'xor
     (f-or! 'xor
       (in-subn
        (f-cons #:bxor (f-cons (Ds and_expr)
                               (ff+ (f-seq ws "^" ws (e-op (Ds xor_expr)))))))
       (Ds and_expr))
     mk-id)))

(set! and_expr
  (wwrap
   (p-freeze 'and     
     (f-or! 'and
       (in-subn
        (f-cons #:band (f-cons (Ds shift_expr) 
                               (ff+ (f-seq ws "&" ws (e-op (Ds and_expr)))))))
       (Ds shift_expr))
     mk-id)))

(set! shift_expr
   (wwrap
   (p-freeze 'shift
     (f-or! 'shift
       (in-subn
        (f-or!
         (f-list #:<< (Ds arith_expr) ws "<<" ws (e-op (Ds shift_expr)))
         (f-list #:>> (Ds arith_expr) ws ">>" ws (e-op (Ds shift_expr)))))
       (Ds arith_expr))
     mk-id)))

(set! arith_expr
  (wwrap
   (p-freeze 'arith
     (f-or! 'arith
       (in-subn
        (f-or!
         (f-list #:+ (Ds term) (f-seq 'rest ws "+" ws (e-op (Ds arith_expr))))
         (f-list #:- (Ds term) (f-seq 'rest ws "-" ws (e-op (Ds arith_expr))))))
       (f-seq 'single_term (Ds term)))
     mk-id)))

(set! factor
  (wwrap
   (p-freeze 'factor
     (f-or! 'factor
       (in-subn
        (f-or!
         (f-list #:u+ (f-seq ws "+" ws (e-op (Ds factor))))
         (f-list #:u- (f-seq ws "-" ws (e-op (Ds factor))))
         (f-list #:u~ (f-seq ws "~" ws (e-op (Ds factor))))))
       (Ds power))
     mk-id)))

(set! term
  (wwrap
   (p-freeze 'term
     (f-or!
      (in-subn
       (f-or! 'term
              (f-list #:@  (Ds factor) ws (f-tag "@")  ws (e-op (Ds term)))
              (f-list #:*  (Ds factor) ws (f-tag "*")  ws (e-op (Ds term)))
              (f-list #:// (Ds factor) ws (f-tag "//") ws (e-op (Ds term)))
              (f-list #:/  (Ds factor) ws (f-tag "/")  ws (e-op (Ds term)))
              (f-list #:%  (Ds factor) ws (f-tag "%")  ws (e-op (Ds term)))))
      (f-seq 'single-factor (Ds factor)))
     mk-id)))

(set! power
  (wwrap
   (p-freeze 'power
            (f-cons* 'power #:power
                    (f-or
                     (f-seq  (Ds out-sub)
                             (f-list (Ds atom)
                                     (ff* (Ds trailer)))
                             e: ws)
                     FALSE)
                    (Ds atom)
                    (ff* (Ds trailer))
                    (f-or! (f-seq ws "**" ws (e-op factor))
                           FALSE))
            mk-id)))

(set! trailer
  (f-or! 'trailer
	 (f-seq ws "(" ws nl+ (ff? (Ds arglist)) ws (f? ",") ws nl-
		(f-or! ")" (e-it "missing right paranthesis")) ws)
	 (f-seq ws "[" ws nl+ (Ds subscriptlist) ws nl-
		(f-or "]" (e-it "missing right bracket")) ws)
	 (f-seq ws "." ws (f-or! identifier (e-it "no identifier")) ws)))

(set! atom
 (wwrap
  (p-freeze 'atom
   (f-or! 'atom
   (f-seq #:null    ws "(" ")"  ws)

   (f-cons
    #:tuple
    (f-or!
     (f-seq "(" ws nl+ ws (ff? (Ds testlist_comp1)) "," ws nl-
	    ")" ws)
     (f-seq "(" ws nl+ (ff? (Ds testlist_comp+))
	    (f-or! (f-seq ws (f-tag ",") ws)
		   f-true)
            ws nl- ")" ws)))

   (f-cons
    #:subexpr
    (f-seq 'sub
	   "(" ws  nl+ (ff? (f-or! 'or
                                   (Ds yield_expr) (Ds testlist_comp)))
           ws nl- (f-or! ")" (e-it "missing right paranthesis")) ws))
   (f-cons
    #:list
    (f-seq "[" ws nl+ (ff? (Ds testlist_comp))
           (f-or! (f-seq ws (f-tag ",") ws)
                  f-true) ws nl-
                  (f-or! "]" (e-it "missing right bracket")) ws))
   (f-cons
    #:dict
    (f-seq "{" nl+ (ff? (Ds dictorsetmaker))
           (f-or! (f-seq ws (f-tag ",") ws)
                  f-true)
           ws nl-
	   (f-or! "}" (e-it "missing right brace")) ws))
   
   scm
   (f-list #:bytes  bytes)
   (f-list #:string (ff+ string))
   (f-seq 'identifier identifier)
   (f-seq 'number number)
   (f-seq #:...     ws (f-tag "...")   nc ws)
   (f-seq #:None    ws (f-tag "None")  nc ws)
   (f-seq #:True    ws (f-tag "True")  nc ws)
   (f-seq #:False   ws (f-tag "False") nc ws))
  mk-id)))

(set! testlist_comp
  (f-cons
   (f-or! star_expr test)
   (f-or!
    (f-cons (Ds comp_for) (f-out '()))
    (f-seq
     (ff* (f-seq ws "," ws (f-or! star_expr test)))))))

(set! testlist_comp+
  (f-cons
   (f-or! star_expr test)
   (f-or!
    (f-cons (Ds comp_for) (f-out '()))
    (f-seq
     (ff+ (f-seq ws "," ws (f-or! star_expr test)))))))

(set! testlist_comp1
  (f-list (f-or! star_expr test)))

(set! sliceop
  (f-seq ":" (ff? test 'None)))

(set! subscript 
  (f-or 'subscript
        (in-sub
         (f-list (f-seq     (ff? test 'None)   )
                 (f-seq ":" (ff? test 'None)   )
                 (f-seq     (ff? sliceop 'None))))
        (f-list test FALSE FALSE)))

(set! subscriptlist
  (f-cons* 'subscriptlist
   #:subscripts
   (Ds subscript)
   (f-seq (ff* (f-seq "," subscript)) (f? ","))))


(define exprlist
  (let* ((f1 (f-or expr star_expr))
         (f2 (f-list #:sub "(" (Ds exprlist) ")"))
         (f  (f-or! f2 f1)))
    (f-cons f (f-seq (ff* (f-seq "," f)) (f? ",")))))

(set! testlist
  (f-cons
   (f-seq test ws)
   (f-seq (ff* (f-seq "," ws test ws)) (f? ","))))
				
(set! dictorsetmaker
  (in-sub
   (let ((f (f-list #:e test (f-seq ":" test ws))))
     (f-or!
      (f-list  f    (Ds comp_for))
      (f-cons  f    (f-seq (ff* (f-seq ws "," ws f))))
      (f-list  test (Ds comp_for))
      (f-cons  test (ff* (f-seq ws "," ws test)))))))

(set! classdef
  (f-list
   #:classdef
   (f-seq "class" identifier)
   (ff? (f-seq! (f-seq "(" nl+ (ff? (Ds arglist) '()) nl- ")")))
   (f-seq ":" suite)))
      
(set! arglist
  (f-list 'arglist
          #:arglist
          (f-or!
           (f-cons (Ds argument)
                   (ff* (f-seq ws "," ws (Ds argument))))
           (f-out '()))))

(set! argument
  (f-or!
   (f-list #:** "**"  test)
   (f-list #:*  "*"  test)
   (f-list #:=  test (f-seq "=" test))
   (f-list #:comp test (ff? (Ds comp_for)))
   (f-list test)))

(define comp_iter (f-or! (Ds comp_for) (Ds comp_if)))
(set! comp_for  (f-list #:cfor
                        (f-seq "for" exprlist)
                        (f-seq "in"  or_test)
                        (ff? comp_iter)))
(set! comp_if   (f-list #:cif
                        (f-seq "if" test_nocond)
                        (ff? comp_iter)))

(set! yield_expr
  (f-or (f-list #:yield (f-seq ws (f-tag "yield") nc ws (ff? (Ds yield_arg))))
        (f-list #:yield "yield" ":" identifier
                (ff? (Ds yield_arg)))))

(set! yield_arg
  (f-or!
   (f-list #:from (f-seq ws (f-tag "from") nc ws test))
   (f-list #:list testlist)))

(define wsnl2 (f* (f-or ws+ f-nl)))
(define input (f-seq 
               (ff+ (f-seq
                     wsnl
                     (f-or! (f-seq indent= simple_stmt) 
                            (f-seq indent= compound_stmt wsnl))))
               (f-seq wsnl2 f-eof)))
(define e-input (f-seq test f-eof))
               
(define (p str)
  (catch #t
    (lambda ()
      (with-fluids ((*whitespace* (f* w)))
        (parse str input)))
    (lambda x
      (format #t "parse error: ~a~%" x)
      #f)))

(define (pe str)
  (catch #t
    (lambda ()
      (with-fluids ((*whitespace* (f* w)))
        (parse str e-input)))
    (lambda x
      (format #t "parse error: ~a~%" x)
      #f)))

(define (python-parser . l)
  (catch #t
    (lambda ()
      (with-fluids ((*whitespace* (f* w)))
        (ppp (apply parse (append l (list (f-seq nl ws single_input ws)))))))
    (lambda x
      (format #t "parse error: ~a~%" x)
      #f)))


