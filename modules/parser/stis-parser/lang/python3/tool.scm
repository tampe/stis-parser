;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (parser stis-parser lang python3 tool)
  #:use-module (ice-9 pretty-print)
  #:use-module (parser stis-parser scanner)
  #:use-module (parser stis-parser slask)
  #:use-module ((parser stis-parser)
		#:select (setup-parser
			  f-nl f-nl! f-skip			  
			  *current-file-parsing*
			  make-file-reader file-next-line file-skip))
  #:use-module (parser stis-parser macros)
  #:re-export (f-nl f-nl! f-skip)
  #:export (f-seq f-seq! f-or f-or! f-not f-not! f-true f-false f-cons f-cons*
		  f-list INDENT <p-lambda> f* ff* ff? f? ff+ f+ f-let
		  f-reg f-reg! f-tag f-tag! f-eof f-out f-and f-and! f-seek
		  mk-token p-freeze parse f-append XL X N M
		  .. xx <p-cc> <p-define> f-scope f-char gg?
		  f-pk))

;; Preliminary
(define do-print (slask-it #f))
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when do-print
       (pretty-print (syntax->datum x)))
     x)))

 
(begin
  (define-parser-tool (<p-lambda> (X XL N M INDENT)) <p-define> .. 
    xx <p-cc>)


  (make-scanner-tools <p-lambda> <fail> <p-cc> <succeds> .. 
                      (X XL N M INDENT)
                      (c) (d)
                      s-false s-true s-mk-seq s-mk-and s-mk-or)

  ;; Sets up a standar parser functionals with INDENT field added
  (setup-parser
   <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx
   X XL ((N 0) (M 0) (INDENT (list 0)))
   s-false s-true s-mk-seq s-mk-and s-mk-or))
