;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (parser stis-parser lang python3 formatter)
  #:export (f-formatter))
  
(define (f-formatter e) (lambda x (error "python formatter not defined")))
