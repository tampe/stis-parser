;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (parser stis-parser lang C-parser)
  #:use-module (parser stis-parser macros)
  #:use-module ((parser stis-parser) #:select (*whitespace*))
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (parser stis-parser)
  #:use-module (parser stis-parser operator-parser)
  #:export (p pp c))

(define ppp
  (case-lambda
    ((s x)
     (pretty-print `(,s ,(syntax->datum x)))
     x)
    ((x)
     (pretty-print (syntax->datum x))
     x)))

(define ws (f* (f-or! (f-char #\newline)
                      (f-char #\space)
                      (f-char #\tab))))

(define f-digit   (f-reg! "[0-9]"))
(define f-integer (f+ f-digit))
(define f-decimal (f-seq f-integer
			 (f-or! (f-seq (f-tag! ".")
                                       (f* f-digit))
                                f-true)))
(define f-exponent (f-seq f-decimal 
			  (f-char #\e)
			  (f-or! (f-char! #\+)
                                 (f-char! #\-)
                                 f-true)
			  f-decimal))

(define hex   (f-list (mk-token (f-seq (f-tag "0") (f-reg "[xX]"))
                                (lambda x "#x"))
                      (mk-token (f+ (f-reg! "[0-9abcdefABCDEF]")))))
(define dec   (f-list (f-out "")
                      (mk-token (f+ (f-reg! "[0-9]")))))

(define bin   (f-list (mk-token (f-seq (f-tag "0") (f-reg "[bB]"))
                                (lambda x "#b"))
                      (mk-token (f+ (f-reg! "[0-1]")))))

(define oct   (f-list (mk-token (f-seq (f-tag "0") (f-reg "[oO]"))
                                (lambda x "#o"))
                      (mk-token (f+ (f-reg! "[0-7]")))))

(define int    (mk-token (f-reg "[iI]") (lambda x 'i)))
(define long   (mk-token (f-reg "[lL]") (lambda x 'l)))
(define short  (mk-token (f-reg "[sS]") (lambda x 's)))
(define char   (mk-token (f-reg "[cC]") (lambda x 'c)))
(define uint   (mk-token (f-seq (f-reg "[uU]") (f-reg "[iI]"))
                         (lambda x 'ui)))
(define ulong  (mk-token (f-seq (f-reg "[uU]") (f-reg "[lL]"))
                         (lambda x 'ul)))
(define ushort (mk-token (f-seq (f-reg "[uU]") (f-reg "[sS]"))
                         (lambda x 'us)))
(define uchar  (mk-token (f-seq (f-reg "[uU]") (f-reg "[cC]"))
                         (lambda x 'uc)))


(define postfix  (f-or! int long char uint ulong ushort uchar (f-out 'i)))
(define prefix   (f-or! hex bin oct dec))
(define integer  (mk-token-raw (f-list prefix postfix)
                               (lambda (x)
                                 (match x
                                   (((pre i) post)
                                    (list
                                     #:integer
                                     (string->number
                                      (string-append pre i))
                                     post))))))
                                 
(define float     (mk-token (f-reg "[fF]") (lambda x 'f)))
(define double    (mk-token (f-reg "[dD]") (lambda x 'd)))
(define postfloat (f-or! float double (f-out 'd)))
(define exponent-body  (mk-token f-exponent string->number))
(define exponent  (f-list #:exp exponent-body postfloat))
(define number    (f-or! exponent integer))


(define keywords
  '(break case char const continue default do double else enum extern 
          float for goto if int long register return short signed sizeof 
          static struct switch typedef union unsigned void volatile while))


;; C Symbols
(define f-head     (f-reg! "[_a-zA-Z]"))
(define f-rest     (f-reg! "[_a-zA-Z0-9]"))
(define f-symbol   (mk-token (f-seq f-head (f* f-rest)) string->symbol))
(define symbol (f-seq f-symbol (f-checkr (lambda (x) (not (memq x keywords))))))

;; C Strings
(define stringbody
  (f-or! (f-reg! "[^\"\\]")
         (f-seq (f-tag "\\n")  (f-addchar #\newline))
         (f-seq (f-tag "\\t")  (f-addchar #\tab))
         (f-seq (f-tag "\\")   (f-reg! "."))))
         
	 
(define f-string  (f-seq (f-tag "\"") (f* stringbody) (f-tag "\"")))
(define string (mk-token f-string))

(define atom
  (p-freeze 'atom (f-or! string symbol number) (lambda (s in o) o)))

;; C operator parser

(define (mk-c-expression ws atom)
  (define C-call (f-seq (D expr) ")"))
  (define C-aref (f-seq (D expr) "]"))
  (define C-?    (f-seq (D expr) ":"))
  (define sub    (f-seq "(" (D expr) ")"))
  (define atom2  (f-or! sub atom))

  (define *ops* (make-opdata))  
  (add-operator *ops* 'yf  "(" 1  C-call)
  (add-operator *ops* 'yf  "[" 2  C-aref)
  (add-operator *ops* 'fy  "(" 3  C-call)
  (add-operator *ops* 'yfx "?" 15 C-?)


  (for-each
   (lambda (x)
     (match x
       ((a b c) (add-operator *ops* a b c ws))))
   '((fy  ++     2) (fy  --     2) (fy  "."    2) (xfy  ->    2)

     (yf  ++     3) (yf  --     3) (fy  +      3) (fy  -      3)
     (fy  !      3) (fy  ~      3) (fy  *      3) (fy  &      3)
     (fy  sizeof 3)

     (xfy *      5) (xfy /      5) (xfy %      5)
   
     (xfy +      6) (xfy -      6)
   
     (xfy <<     7) (xfy >>     7)
     
     (xfy <      8) (xfy >      8) (xfy <=     8) (xfy >=     8)
   
     (xfy ==     9) (xfy !=     9)
     
     (xfy &      10)
     
     (xfy ^      11)
   
     (xfy "|"    12)
   
     (xfy &&     13)

     (xfy "||"   14)
   
     (yfx =      16) (yfx +=     16) (yfx -=     16) (yfx *=     16)
     (yfx /=     16) (yfx %=     16) (yfx <<=    16) (yfx >>=    16)
     (yfx &=     16) (yfx ^=     16) (yfx "|="   16)
   
     (xfy ","      18)))

  ;;cludge to avoid compiler stuipidities
  (let ((expr ((mk-operator-expression ws atom2 f-false *ops*) 18)))
    (values expr *ops*)))


(define expr (mk-c-expression ws atom))


(define (p str)
  (with-fluids ((*whitespace* ws))
    (ppp (parse str (f-seq (f? f-nl) expr f-eof)))
    (if #f #f)))


(define f-scheme (f-list #:scm "[" (mk-token (f+ (f-not! (f-tag "]")))) "]"))
(define f-eval   (f-or! f-scheme expr))

(define (unfold-comma x)
  (match x
    (((_ "," _ _) a b)
     (cons a (unfold-comma b)))
    (x (list x))))

(define (assign lp a b sum)
  (call-with-values (lambda () (lp a))
    (lambda (a . l)
      (when (null? l)
        (error (format #f "wrong asign expression ~a" a)))
      (let ((r (gensym "r")))
        `(let ((,r ,(sum a (lp b))))
           ,((car l) r)
           ,r)))))

(define (d x) (if (eq? x 0) #f x))
(define q `(@@ (parser stis-parser lang C-parser) d))

(define (c x)
  `(call-with-values
       (lambda ()
         ,(let lp ((x (pp x)))
            (match x
              ((#:scm scm)
               (with-input-from-string (string-append "(" scm ")") read))
              ((('fy _ "(" a) b _ _)
               (if (symbol? b)
                   `(,b ,(map lp (unfold-comma a)))
                   (error (format #f "unhandled application ~a" b))))
              
              ((('yf _ "(" (or float double)) b _ _)
               `(exact->inexact ,(lp b)))
              
              ((('yf _ "(" (or int long)) b _ _)
               `(inexact->exact ,(lp b)))
              
              (((_ _ "?" b) a c _ _)
               `(if (,q ,(lp a)) ,(lp b) ,(lp c)))
              
              (((_ _ "+" _) a _ _)
               (lp a))
              
              (((_ _ "-" _) a _ _)
               `(- ,(lp a)))
              
              (((_ _ "%" _) a b _ _)
               `(modulo ,(lp a) ,(lp b)))
              (((_ _ "+" _) a b _ _)
               `(+ ,(lp a) ,(lp b)))
              (((_ _ "-" _) a b _ _)
               `(- ,(lp a) ,(lp b)))
              (((_ _ "*" _) a b _ _)
               `(* ,(lp a) ,(lp b)))
              (((_ _ "/" _) a b _ _)
               `(/ ,(lp a) ,(lp b)))
              
              (((_ _ "<" _) a b _ _)
               `(< ,(lp a) ,(lp b)))
              (((_ _ "<=" _) a b _ _)
               `(<= ,(lp a) ,(lp b)))
              (((_ _ ">" _) a b _ _)
               `(> ,(lp a) ,(lp b)))
              (((_ _ ">=" _) a b _ _)
               `(>= ,(lp a) ,(lp b)))
              
              (((_ _ "||" _) a b _ _)
               `(or (,q ,(lp a)) (,q ,(lp b))))
              (((_ _ "&&" _) a b _ _)
               `(and (,q ,(lp a)) (,q ,(lp b))))
              (((_ _ "!" _) a _ _)
               `(not (,q ,(lp a))))
              
              (((_ _ "&" _) a b _ _)
               `(logand ,(lp a) ,(lp b)))
              (((_ _ "|" _) a b _ _)
               `(logior ,(lp a) ,(lp b)))
              (((_ _ "^" _) a b _ _)
               `(logxor ,(lp a) ,(lp b)))
              (((_ _ "<<" _) a b _ _)
               `(ash ,(lp a) ,(lp b)))
              (((_ _ ">>" _) a b _ _)
               `(ash ,(lp a) (- ,(lp b))))
              (((_ _ "~" _) a _ _)
               `(lognot ,(lp a)))
              
              
              ((('xfy _ "==" _) a b _ _)
               `(equal ,(lp a) ,(lp b)))
              ((('xfy _ "!=" _) a b _ _)
               `(not (equal ,(lp a) ,(lp b))))
              
              (((_ _ "++" _) a _ _)
               (call-with-values (lambda () (lp a))
                 (lambda (a f)
                   (values `(begin ,(f `(+ ,a 1)) ,a) f)))) 
              
              (((_ _ "--" _) a _ _)
               (call-with-values (lambda () (lp a))
                 (lambda (a f)
                   (values `(begin ,(f `(- ,a 1)) a) f)))) 
              
              (((_ _ "=" _) a b _ _)
               (assign lp a b (lambda (a b) b)))
              
              (((_ _ "%=" _) a b _ _)
               (assign lp a b (lambda (a b) `(modulo ,a ,b))))
              (((_ _ "+=" _) a b _ _)
               (assign lp a b (lambda (a b) `(+ ,a ,b))))
              (((_ _ "-=" _) a b _ _)
               (assign lp a b (lambda (a b) `(- ,a ,b))))
              (((_ _ "*=" _) a b _ _)
               (assign lp a b (lambda (a b) `(* ,a ,b))))
              (((_ _ "/=" _) a b _ _)
               (assign lp a b (lambda (a b) `(/ ,a ,b))))
              
              (((_ _ "&=" _) a b _ _)
               (assign lp a b (lambda (a b) `(logand ,a ,b))))
              (((_ _ "|=" _) a b _ _)
               (assign lp a b (lambda (a b) `(logior ,a ,b))))
              (((_ _ "^=" _) a b _ _)
               (assign lp a b (lambda (a b) `(logxor ,a ,b))))
              (((_ _ ">>=" _) a b _ _)
               (assign lp a b (lambda (a b) `(ash ,a (- ,b)))))
              (((_ _ "<<=" _) a b _ _)
               (assign lp a b (lambda (a b) `(ash ,a ,b))))
              
              
              (((_ _ "," _) a b _ _)
               `(begin
                  ,(lp a)
                  ,(lp b)))
              
              ((#:integer x _)
               x)
              (x
               (if (number? x)
                   x
                   (if (symbol? x)
                       (values x (lambda (r) `(define! ',x ,r)))
                       (error (format #f "not handled expersion operator ~a" x))))))))
     (lambda (x . a)
       x)))

(define (pp str)
  (with-fluids ((*whitespace* ws))
    (parse str (f-seq (f? f-nl) f-eval f-eof))))
