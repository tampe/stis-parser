;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (parser stis-parser macros)
  #:use-module (parser stis-parser pre)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 match)
  #:export (define-parser-tool <fail> <succeds>
	     <=> <==> <and> <and!> <and!!> <if> <when> <or> <cc> <not> <cut>
	     <syntax-parameterize> <lambda> <var> <format> <pp-dyn>
	     <scm> <with-fail> <with-s> <values> <_> <apply> <code>
	     unify unify== mk-variable var? S CUT P CC <with-bind> 
	     parse<> <run> <pp> scm lookup))

(define (pp x)
  (pretty-print x)
  x)

(define (var? x s) (variable? (lookup x s)))
(define (lookup y s)
  (let lp ((y y))
    (let ((x (assq y s)))
      (if x
	  (let ((x (cdr x)))
	    (if (var? x s)
		(lp (assq x s))
		x))
	  y))))

(define (unify x y s)
  (let ((x (lookup x s))
	(y (lookup y s)))
    (cond
     ((eq? x y) s)
     ((var? x s)
      (cons (cons x y) s))
     ((var? y s)
      (cons (cons y x) s))
     ((pair? x)
      (if (pair? y)
	  (let ((s2 (unify (car x) (car y) s)))
	    (if s2
		(unify (cdr x) (cdr y) s2)
		#f))
	  #f))
     ((pair? y)
      #f)
     (else
      (if (equal? x y)
          s
          #f)))))

(define (scm- x s)
  (let ((x (lookup x s)))
    (cond
     ((pair? x)
      (cons (scm- (car x) s) (scm- (cdr x) s)))
     (else
      x))))

(define-syntax scm
  (syntax-rules ()
    ((_ x)   (scm- x S))
    ((_ x y) (scm- x y))))

(define (unify== x y s)
  (let ((x (lookup x s))
	(y (lookup y s)))
    (cond
     ((eq? x y) s)
     ((var? x s)
      #f)
     ((var? y s)
      #f)
     ((pair? x)
      (if (pair? y)
	  (let ((s2 (unify== (car x) (car y) s)))
	    (if s2
		(unify== (cdr x) (cdr y) s2)
		#f))
	  #f))
     ((pair? y)
      #f)
     (else
      (equal? x y)))))

(define i 0)
(define mk-variable
  (lambda ()
    (set! i (+ i 1))
    (make-variable i)))

(define-syntax-rule (<scm> x)     
  (let ((u x))
    (if (number? u)
	u
	(scm u S))))

(define-syntax-rule (fl-let (cut s p cc) code ...)
  (syntax-parameterize ((S   (identifier-syntax s))
			(P   (identifier-syntax p))
			(CC  (identifier-syntax cc))
			(CUT (identifier-syntax cut)))
    code ...))


(define-syntax-parameter S   
  (lambda (x) (error "S should be bound by fluid-let")))
(define-syntax-parameter P   
  (lambda (x) (error "P should be bound by fluid-let")))
(define-syntax-parameter CC  
  (lambda (x) (error "CC should be bound by fluid-let")))
(define-syntax-parameter CUT 
  (lambda (x) (error "CUT should be bound by fluid-let")))


(eval-when (compile eval load)
  (define-syntax <_>
    (lambda (x)
      (syntax-case x ()
	((_ . _) (error "<_> in logical is not a function"))
	(_       #'(mk-variable))))))

(define-log <pp>
  (syntax-rules ()
    ((_ (cut s p cc) x) 
     (fl-let (cut s p cc)
        (pp (scm x s))
        (parse<> (cut s p cc) <cc>)))))

(define-log <with-bind> 
  (lambda (x)
    (syntax-case x ()
      ((_ w ((X I) ...) code ...)
       (with-syntax (((id ...) (generate-temporaries #'(X ...))))
         #'(let  ((id I) ...)
             (<syntax-parameterize> w ((X (lambda x #'id)) ...)
               (<and> code ...))))))))

(define-log <code>
  (syntax-rules ()
    ((_ w code ...)
     (fl-let w
        code ...
	(<cc> w ())))))

(define-log <var>
  (syntax-rules ()
    ((_ (cut s p cc) (v ...) code ...) 
     (let ((v (mk-variable)) ...)
       (fl-let (cut s p cc)
	 (<and> (cut s p cc) code ...))))))

(define-and-log <cc>
  (syntax-rules ()
    ((<cc> (cut s p cc) ())
     (cc s p))
    ((<cc> (cut s p cc) () a ...)
     (<and> (cut s p cc) a ...))

    ((<cc> (cut s p cc) (_ . l))
     (fl-let (cut s p cc) 
	(cc s p . l)))

    ((<cc> (cut s p cc) (_ . l) a ...)
     (<and> (cut s p cc) (<code> (fl-let (cut s p cc) . l))
	    a ...))))

(eval-when (compile load eval)
(define (ander x)
  (syntax-case x (<and>)
    ((a ... (<and> . l))
     (if (complex-and? #'l)
	 x
	 (append #'(a ...) (ander #'l))))
    ((a ...)
     #'(a ...)))))

(eval-when (compile load eval)
(define (complex-and? x) #t))


(eval-when (compile load eval)
(define (fand s p cc cut fs)
  (if (pair? fs)
      (let ((ccc (lambda (s p) (fand s p cc cut (cdr fs)))))
	((car fs) s p ccc cut))
      (cc s p))))

(define-log <syntax-parameterize>
  (syntax-rules ()
    ((_ w a code ...)
     (syntax-parameterize a 
       (parse<> w (<and> code ...))))))

(define-syntax-rule (cc-let (cc) code ...)
  (syntax-parameterize ((CC  (identifier-syntax cc)))
    code ...))

(define-log <and>
  (lambda (x)
    (syntax-case x ()
      ((_ ww aa ...)
       (if (not (complex-and? #'(aa ...)))
	   (syntax-case (list #'ww (ander #'(aa ...))) ()
	     (((cut s p cc) (aaa ...))
	      #'(fand s p cc cut 
		      (list
		       (lambda (ss pp ccc ccut)
			 (parse<> (ccut ss pp ccc) aaa))
		       ...))))
			 
    (syntax-case x ()
      ((_ w)
       #'(parse<> w <cc>))

      ((_ w (n . l) e2 ...)
       (and-log-macro? (syntax n))
       #'(n w (n . l) e2 ...))

      ((_ w n e2 ...)
       (and-log-macro? #'n)
       #'(n w () e2 ...))

      ((_ meta       e1) 
       #'(parse<> meta e1))

      ((_ (cut s pr cc) (a b ...) e2 ...)
       (log-code-macro? #'a)
       #'(a (cut s pr (parse<> (cut s pr cc) 
                        (<and> e2 ...)))
                 b ...))

      ((_ (cut s pr cc) e1 e2 ...)  
       #'(let ((ccc (lambda (ss pr2)
		      (fl-let (cut ss pr2 cc)
			(parse<> (cut ss pr2 cc) (<and> e2 ...))))))
         (cc-let (ccc) 	   
           (parse<> (cut s pr ccc) e1))))))))))

(define-log <and!>
  (syntax-rules ()
    ((_ w) (parse<> w <cc>))
    ((_ (cut s p cc) a ...) 
     (let ((ccc (lambda (ss pp . l) 
		  (apply cc ss p l))))
       (parse<> (cut s p ccc) (<and> a ... ))))))

(define-log <and!!>
  (lambda (x)
    (syntax-case x ()
      ((_ w)
       #'(parse<> w <cc>))

      ((_ (cut s pr cc) (n . l) e2 ...)
       (if (and-log-macro? #'n)
           #t
           #f)
       #'(n (cut s pr cc) (n . l) (<with-fail> pr (<and!!> e2 ...))))

      ((_ (cut s pr cc)  n e2 ...)
       (if (and (identifier? #'n) (and-log-macro? #'n))
           #t
           #f)
       #'(n  (cut s pr cc) (<with-fail> pr (<and!!> e2 ...))))


      ((_ meta       e1       ) 
       #'(parse<> meta (<and!> e1)))

      ((_ (cut s pr cc) (a b ...) e2 ...)
       (log-code-macro? (syntax a))
       #'(a (cut s pr (parse<> (cut s pr cc) 
			 (<and!!> e2 ...)))
                 b ...))

      ((_ (cut s pr cc) e1 e2 ...)  
       #'(let ((ccc (lambda (ss pr2) 
		      (parse<> (cut ss pr cc) 
			(<and!!> e2 ...)))))
           (parse<> (cut s pr ccc) e1))))))


(define-and-log <ret>
  (syntax-rules ()
    ((_ (cut s p cc) (_ code ... r) a ...)
     (let ((cc2 (lambda (s2 p2)
		  (parse<> (cut s2 p2 cc)
		    (<and> a ...)))))
       (fl-let (cut s p cc2) 
	 (begin code ... (scm r S)))))))
	 
(define-and-log <cut> 
  (syntax-rules ()
    ((_ (cut s p cc) () a ...)
     (parse<> (cut s cut cc) 
       (<with-fail> cut
	  (<and> a ...))))
    ((_ (cut s p cc) (_ . l) a ...)
     (<and> (cut s p cc) 
          (<with-fail> cut 
	      (<and> . l)) a ...))))



(define-and-log <fail>
  (syntax-rules ()
    ((_ (cut s p cc) (    ) a ...) (p))
    ((_ (cut s p cc) (_ pp) a ...) (pp))))

(define-log <succeds>
  (syntax-rules ()
    ((_ (cut s p cc) g ...)
     (let* ((ccc (lambda (ss pp) (cc s p))))
       (parse<> (cut s p ccc) (<and> g ...))))))

(define-log <not>
  (syntax-rules ()
    ((_ (cut s p cc) code)
     (let ((cc2 (lambda x (p)))
	   (p2  (lambda () (cc s p))))
       (<and> (p2 s p2 cc2) code)))))

(define-log <or>
  (syntax-rules (****)
    ((_ meta   ) (parse<> meta <fail>))
    
    ((_ meta e1) (parse<> meta e1))

    ((_ (cut ss pr cc) . l)
     (or-aux ss (cut ss pr cc) . l))))

(define-syntax or-aux
  (syntax-rules ()
    ((_ ss (cut s p cc) a)
     (begin
       (parse<> (cut ss p cc) a)))
    ((_ ss (cut s p cc) a . as)
     (let ((pp (lambda ()
		 (or-aux ss (cut s p cc) . as))))
       (parse<> (cut s pp cc) a)))))


(define-log <scm-if>
  (syntax-rules ()
    ((_ (cut s p cc) pred a)
     (if pred
         (<and> (cut s p cc) a)
         (p)))

    ((_ (cut s p cc) pred a b)
     (if pred
         (<and> (cut s p cc) a)
         (<and> (cut s p cc) b)))))

(define (->list s l)
  (let lp ((l l))
    (let ((l (lookup l s)))
      (if (pair? l)
	  (cons (car l) (lp (cdr l)))
	  l))))

(define-syntax-rule (<apply> s p cc f x ... l)
  (apply (lookup f s) s p cc x ... (->list s l)))


(define-log <when>
  (syntax-rules ()
    ((_ (cut s p cc) pred code ...)
     (fl-let (cut s p xcc)
	(if pred 
	    (parse<> (cut s p cc) (<and> code ...))
	    (parse<> (cut s p cc)  <fail>))))))

(define-log <if>
  (syntax-rules ()
    ((_ meta p a)
     (parse<> meta 
	      (let ((s S))
		(<and!> p) 
		a)))
    ((_ (cut s p cc) pred a b)
     (let* ((pp (lambda ()
		  (parse<> (cut s p cc) b))))
       (parse<> (cut fr pp cc)
	 (<and> 
	  pred 
	  (<with-fail> p a)))))))

(define-log <let>
  (syntax-rules ()
    ((_ (cut s p cc) () code ...)
     (let () (parse<> (cut s p cc) (<and> code ...))))
    ((_ (cut s p cc) (v ...) code ...)
       (letrec ((ccc (lambda (ss pp)
                       (fl-let (cut s p cc)
                         (let (v ... )
                           (parse<> (cut ss pp cc) (<and> code ...)))))))
         (ccc s p)))))

(define-log <let*>
  (syntax-rules ()
    ((_ (cut s p cc) () code ...)
     (let () (parse<> (cut s p cc) (<and> code ...))))

    ((_ (cut s p cc) (v ...) code ...)
       (letrec ((ccc (lambda (ss pp)
                       (fl-let (cut s p cc)                       
                         (let* (v ... )
                           (parse<> (cut ss pp cc) (<and> code ...)))))))
         (ccc s p)))))

(define-log <letrec>
  (syntax-rules ()
    ((_ (cut s p cc) ((v . lam) ...) code ...)
     (fl-let (cut s p cc)
        (letrec ((v . lam) ...)
           (parse<> (cut s p cc)
                (<and> code ...)))))))

(define-syntax <with-log>
  (syntax-rules ()
    ((_ (cut s p cc) code ...) 
     (fl-let (cut s p cc)
       (parse<> (cut s p cc) (<and> code ...))))
    ((_ (s p cc) code ...)
     (<with-log> (p s p cc) code ...))))

(define-syntax <lambda>
  (syntax-rules ()
    ((_ as code ...)
     (lambda (<S> <Cut> <CC> . as)
       (<with-log> (<S> <Cut> <CC>) 
	 (<and> code ...))))))

(define-log <pp-dyn>
  (syntax-rules ()
    ((_ (cut s p cc) a b)   
     (let ((pf (lambda () (pp (scm b s)) (p))))
       (pp (scm a s)) 
       (cc s pf)))))

(define-log <format>
  (syntax-rules ()
    ((_ (cut s p cc) stream str a ...)
     (begin 
       (format stream str (scm a s) ...)
       (parse<> (cut s p cc) <cc>)))))

(define-syntax tr-pat
  (lambda (x)
    (syntax-case x (quote unquote)
      ((_ (unquote x)) #'x)
      ((_ (x . l))     (list #'cons #'(tr-pat x) #'(tr-pat l)))
      ((_ ())          #''())
      ((_ x)
       (eq? '_ (syntax->datum #'x))
       #'(mk-variable))
      ((_ x)           #'x))))

(define-log <unify>
  (syntax-rules ()
    ((_ (cut s p cc) unify X Y)
     (let* ((ss s)
	    (ss (unify X Y ss)))
       (<when> (cut ss p cc) ss <cc>)))))

(define-log <=>q
  (syntax-rules ()
    ((_ w . l)
     (fl-let w (<=>qq w . l)))))

(define-syntax <=>qq
  (syntax-rules (unquote quote _)

    ((_ wc q _   x)
     (parse<> wc <cc>))

    ((_ wc q x   _)
     (parse<> wc <cc>))

    ((_ wc q ()  ()) 
     (parse<> wc <cc>))
    
    ((_ wc q ()  X)
     (<=>q wc q '() X))

    ((_ wc q X   ())
     (<=>q wc q '() X))

    ((_ wc q (X) (Y))
     (<=>q wc q X Y))

    ;;unquote logic so that we can evaluate forms in the unification
    ((_ wc unify (unquote X)  (unquote Y))
     (<unify> wc unify X Y))

    ((_ meta unify (unquote X)  (quote   Y))
     (<unify> meta unify X (quote Y)))

    ((_ wc unify (unquote X)  (Y . L)    )   
     (<unify> wc unify (tr-pat (Y . L)) X <cc>))

    ((_ wc unify (unquote X)  Y          )
     (<unify> wc unify X Y))

    ((_ wc q Y            (unquote X))   
     (<=>q wc q (unquote X) Y))

    ;; quote logic to protect quoted forms from destructioning
    ((_ wc unify (quote X)  (quote Y))
     (<unify> wc unify (quote X) (quote Y)))

    ((_ wc u     (quote X)  (Y . L)    ) 
     (<unify> wc u (tr-pat (Y . L)) (quote X)))

    ((_ wc unify (quote X)  Y          )  
     (<unify> wc unify (quote X) Y))

    ((_ wc q Y         (quote X))  
     (<=>q wc q (quote X) Y))


    ((_ wc q     (X . Lx) (Y . Ly))
     (parse<> wc (<and> (<=>q q X Y) (<=>q q Lx Ly))))

    ((_ wc u     X        (Y . Ly))
     (<unify> wc u (tr-pat (Y . Ly)) X))

    ((_ wc u     (X . Lx)    Y)
     (<unify> wc u  (tr-pat (X . Lx)) Y))

    ((_ wc unify X Y)
     (<unify> wc unify X Y))))

(define-log <=>
  (syntax-rules ()
    ((_ wc X Y)
     (<=>q wc unify X Y))))

(define-log <==>
  (syntax-rules ()
    ((_ wc X Y)
     (<=>q wc unify== X Y))))

(define-and-log <values>
  (syntax-rules ()    
    ((_ (cut s pr cc) (<values> vars a ...) e2 ...)
     (let ((lam (lambda (ss pr2 . vars) 
		  (parse<> (cut ss pr2 cc) 
		     (<and>  e2 ...)))))
       (parse<> (cut s pr lam)
	 (<and> a ...))))))

(define-syntax <%p-values%>
  (lambda (x)
  (syntax-case x ()
    ((_ (cut s pr cc) (((P p) ...) vars (fkn . l))  e2 ...)
     #'(parse<> (cut s pr (lambda (ss pr2 p ... . vars) 
                            (syntax-parameterize
                             ((P  (lambda (x)
                                    (syntax-case x ()
                                      ((x . l) 
                                       #'(p . l))
                                      (x #'p))))
                              ...)
                             (parse<> (cut ss pr2 cc) 
                                (<and> e2 ...)))))
                (fkn P ... . l)))
    ((a . l)
     (error 
      (format #f "wrong application of ~a in <and> like constructs of ~a" 
              (syntax->datum #'a)
              (syntax->datum x)))))))

(define-syntax <%q-values%>
  (lambda (x)
  (syntax-case x ()
    ((_ (cut s pr cc) (((P p) ...) vars a ...) e2 ...)
     #'(parse<> (cut s pr (lambda (ss pr2 p ... . vars) 
			  (syntax-parameterize
			   ((P  (lambda (x)
				  (syntax-case x ()
				    ((x . l) 
				     #'(p . l))
				    (x #'p))))
			    ...)
			   (parse<> (cut ss pr2 cc) 
				    (<and> e2 ...)))))
	      (<and> a ...)))
    ((a . l)
     (error 
      (format #f "wrong application of ~a in <and> like constructs of ~a" 
              (syntax->datum #'a) (syntax->datum x)))))))
			       
(define-syntax define-parser-tool
  (lambda (x)
    (syntax-case x ()
      ((_ (lam (X ...)) def .. xx cc)
       (with-syntax (((p ...) (generate-temporaries #'(X ...))))
	 #'(begin
	     (define-syntax-parameter X
	       (lambda  (x) (error
			     (format #f
				     "logical scanner var not bound for ~a"
				     'X))))
	     ...
	     
	     (define-syntax-rule (lam q . code)
	       (<lambda> (p ... . q)
		 (<syntax-parameterize> ((X  (lambda (x)
					   (syntax-case x ()
					     ((x . l) 
					      #'(p . l))
					     (x #'p))))
				     ...)
		    . code)))
	     
	     (define-syntax-rule (def (f . q) . code)
	       (define f (lam q . code)))
	     
	     (define-and-log ..
	       (syntax-rules ()
		 ((_ (cut s pr cc) (_ (fkn . l)))
		  (fkn s pr cc X ... . l))
		 ((_ w (_ () fkn) . es)
		  (<%p-values%> w (((X p) ...) () fkn) . es))
		 ((_ w (_ (x . l) fkn) . es)
		  (<%p-values%> w (((X p) ...) (x . l) fkn) . es))
		 ((_ w (_ x fkn) . es)
		  (<%p-values%> w (((X p) ...) (x)     fkn) . es))))

	     (define-and-log xx
	       (syntax-rules ()
		 ((_ w (_ () . as) . es)
		  (<%q-values%> w (((X p) ...) () . as) . es))
		 ((_ w (_ (x . l) . as) . es)
		  (<%q-values%> w (((X p) ...) (x . l)  . as) . es))
		 ((_ w (_ x . as) . es)
		  (<%q-values%> w (((X p) ...) (x)      . as) . es))))

	     (define-and-log cc 
	       (syntax-rules ()
		 ((_ (cut s pp ccc) (_ . l))
                  (fl-let (cut s pp cc)
                   (ccc s pp X ... . l)))))))))))


(define-syntax parse<>
  (lambda (x)
    (syntax-case x ()
      ((_ . l)
       ;(pp `(parse<> ,@(syntax->datum (syntax l))))
       (syntax (parse2<> . l))))))

(define-log <recur>
  (syntax-rules ()
    ((_ (cut s p cc) n ((w v) ...) code ...)
     (letrec ((n (lambda (ss pp cccc w ...)
                   (<with-log> (cut ss pp cccc) 
                     (<and> code ...)))))
       (parse<> (cut s p cc)
         (n v ...))))))

(define-syntax parse2<>
  (lambda (x)
    (syntax-case x  (if when fast-when cond else case let let* letrec match)
      ((_ meta (match x (p code ...) ...))
       #'(match x (p (<and> meta code ...)) ...))
      
      ((_ meta (letrec . l)) 
       #'(<letrec> meta . l))

      ((_ meta (let (a ...) . l)) 
       #'(<let> meta (a ...) . l))

      ((_ meta (let lp (a ...) . l)) 
       #'(<recur> meta lp (a ...) . l))

      ((_ meta (let* . l)) 
       #'(<let*> meta . l))

      ((_ meta (if p . l)      ) 
       #'(<scm-if> meta p . l))

      ((_ meta (when p . l)    ) 
       #'(<scm-if> meta p (<and> . l)))

      ((_ meta (cond (else a ...) . l))
       #'(<and> meta a ...))

      ((_ meta (cond (p a ...) . l))
       #'(<scm-if> meta p 
            (<and> a ...)
            (cond . l)))

      ((_ meta (cond))
       #'(parse2<> meta <fail>))

      ((m meta (case q (p a ...) ...))
       (with-syntax ((y (datum->syntax #'m (gensym "x"))))
          (with-syntax (((p ...) (map (lambda (x)
					(syntax-case x (else)
					  (else x)
					  ((a ...) #'(or (eq? y a) ...))
					  (a       #'(or (eq? q a)))))
				      #'(p ...))))
              #'(<let> meta ((y q))
		  (cond (p a ...) ...)))))
       
      ;; And code macros are special
      ((_ meta (f a ...)       )
       (and-log-macro? #'f)
       #'(f meta (f a ...)))

      ((_ meta f       )
       (and-log-macro? #'f)
       #'(f meta ()))

      ;;logical macro or function application
      ((_ meta (f ...)         ) 
       #'(dispatch meta (f ...))))))

(define-syntax <%fkn%>
  (syntax-rules ()
    ((_ (cut s pr cc) f a ...)
     (fl-let (cut s pr cc)
      (f s pr cc a ... )))))

(define-log <with-cc>
  (syntax-rules ()
    ((_ (cut s p cc) ccc code ...)
     (let ((cccc ccc))
       (syntax-parameterize ((CC (identifier-syntax cccc)))
          (parse<> (cut s p cccc) (<and> code ...)))))))

(define-log <with-s>
  (syntax-rules ()
    ((_ (cut s p cc) ss code ...)
     (let ((sss ss))
       (syntax-parameterize ((S (identifier-syntax sss)))
          (parse<> (cut sss p cc) (<and> code ...)))))))

(define-log <with-fail>
  (syntax-rules ()
    ((_ (cut s p cc) pp code ...)
     (let ((ppp pp))
       (syntax-parameterize ((P (identifier-syntax ppp)))
          (parse<> (cut s ppp cc) (<and> code ...)))))))

(define-log <with-cut>
  (syntax-rules ()
    ((_ (cut s p cc) cutt code ...)
     (let ((cuttt cutt))
       (syntax-parameterize ((CUT  (identifier-syntax cuttt)))			
         (parse<> (cuttt s p cc) (<and> code ...)))))))


(define-syntax dispatch
  (lambda (x)
    (syntax-case x ()
      ((_ w (n . l))       
       ;(pk (syntax->datum (syntax (n w . l))))
       (if (log-macro? (syntax n))
           (syntax (n w . l))
           (syntax (<%fkn%> w n . l)))))))

(define-syntax <run>
  (syntax-rules ()
    ((<run> 1 (v) code ...)
     (let* ((v (mk-variable))
	    (p  (lambda () '()))
	    (cc (lambda (s pp) (list (scm v s)))))
       (<and> (p '() p cc) code ...)))))
