;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (parser stis-parser operator-parser)
  #:use-module (parser stis-parser macros)
  #:use-module (parser stis-parser)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 vlist)
  #:export (make-opdata add-operator rem-operator
                        generate-operator-tool
                        mk-operator-expression
                        table->assq assq->table mk-fop
                       get-operator get-binops get-unops))


(define (vhash->assoc x)
  (reverse
   (vhash-fold (lambda (s k v) (cons (cons k v) s)) '() x)))
#|
 Enhanced Classic Prolog Operator Parser
 =======================================

 The output format is:
   (op-data x y col row)
   (op-data x   col row)
   (type level name pre-char)

|#

(define null! 'a-very-ugly-hack-of-operator-table-nulling-id)
(define-syntax-rule (my-hash-ref table key default)
  (let ((xx (vhash-assoc key (fluid-ref table))))
    (if xx
	(let ((r (cdr xx)))
          (if (eq? r null!)
              default
              r))
	default)))

(define (to-string name)
  (cond ((string? name) name)
	((symbol? name) (symbol->string name))))

(define (== x)
  (lambda (y)
    (equal? x y)))

(define (table->assq table)
  (define ha (make-hash-table))
  (let lp ((l (vhash->assoc table)))
    (if (pair? l)
        (cond
         ((eq? (cdar l) null!)
          (begin
            (hash-set! ha (caar l) #t)
            (lp (cdr l))))
         ((hash-ref ha (caar l))
          (lp (cdr l)))
         (else
          (cons (car l) (lp (cdr l)))))
        '())))

(define (assq->table a)
  (fold (lambda (x s) 
          (vhash-cons (car x) (cdr x) s)) 
        vlist-null (reverse a)))

(define (rem-operator table type key)
  (let* ((nm (string->list (to-string key)))
	 (k  (car nm))
	 (r  (cdr nm)))
    (let ((x (my-hash-ref table k #f)))
      (when x
	(let ((li (let lp ((x x) (pre '()))
		    (match x
		      ((( (qq1 . (? (== type))) (? (== r)) . qq2) . l)
		       (lp l pre))
		      ((x . l)
		       (lp l (cons x pre)))
		      (() (reverse pre))))))
	  (if (null? li)
	      (fluid-set! table 
                          (vhash-cons    k null! (fluid-ref table)))
	      (fluid-set! table 
                          (vhash-cons    k li    (fluid-ref table)))))))))

(define (get-operator table type key)
  (let* ((nm (string->list (to-string key)))
	 (k  (car nm))
	 (r  (cdr nm)))
    (let ((x (my-hash-ref table k #f)))
      (when x
	(let ((li (let lp ((x x) (pre '()))
		    (match x
		      ( (( (qq1 . (? (== type))) 
                           (? (== r)) . qq2) . l)
                        (lp l (cons (car x) pre)))
		      ((x . l)
		       (lp l pre))
		      (() (reverse pre))))))
          li)))))

(define (make-opdata) (make-fluid vlist-null))

(define (add-operator data type name level lf)
  (let* ((nm       (to-string name))
	 (ch       (string-ref nm 0))
	 (ch-rest  (cdr (string->list nm))))
    (let* ((res1 (my-hash-ref data ch '()))
	   (x    (cons* (cons nm type) ch-rest level lf))
	   (n    (length ch-rest)))
      (define (finish pre tail)
	(let lp ((pre pre) (tail tail))
	  (match pre
	    (()
	     (fluid-set! data (vhash-cons ch tail (fluid-ref data))))
	    ((x . l)
	     (lp l (cons x tail))))))

      (let lp ((res1 res1) (pre '()))	
	(match res1
	  (()
	   (finish pre (list x)))
	  (((and q (qq1 y . qq2)) . u)
	   (if (> (length y) n)
	       (lp u (cons q pre))
	       (finish pre (cons x res1)))))))))

(define (get-binops data op)
  (append
   (get-operator data 'xfx op)
   (get-operator data 'xfy op)
   (get-operator data 'yfx op)))

(define (get-unops data op)
  (append
   (get-operator data 'xf op)
   (get-operator data 'yf op)
   (get-operator data 'fx op)
   (get-operator data 'fy op)))

(define-syntax-rule (generate-operator-tool
                     mk-operator-expression <p-define> <p-lambda> .. <p-cc>)
  (begin
    (define (mk-fop data)
      (define (op-f F)
        (if F
            F
            f-true))

      (define (op-chlist li)
        (letrec ((f (lambda (li) 
                      (if (pair? li)
                          (f-seq (f-char (car li)) (f (cdr li)))
                          f-true))))
          (f li)))
      (lambda (FXS)
        (<p-lambda> (C)
          (.. (c1) (f-read '()))
          (let ((val (my-hash-ref data c1 '())))
            (let lp ((l val))
              (when (pair? l)
                (<and!>
                 (match l
                   ((((name . FX) ch-rest level . F) . u)		    
                    (<or>
                     (when (member FX FXS)
                       (<and!>
                        (.. (c2) ((op-chlist ch-rest) c1))
                        (.. (c3) ((op-f      F)       c2))
                        (<p-cc> (list FX level name c3))))
                     (lp u)))))))))))
    
    (define (mk-operator-expression ws op-atom rest-atom data)
      (define fop  (mk-fop data))
      (define fop1 (fop '(fy fx)))
      (define fop2 (fop '(xfx xfy yfx yf xf)))

  
      (<p-define> (term To PP)
        (<var> (New)
          (<or>
           (<and>
            (.. (c) (ws '()))
            (.. (t) (op-atom '()))
            (.. (rterm t To PP 0)))

           (<and>
            (.. (c) (ws '()))
            (.. (p) (fop1 '()))
            (match p
              (('fx P . _)
               (when (<= P PP)
                 (.. (t) (term New (- P 1)))
                 (.. (rterm  (list p New N M) To PP P))))
              
              (('fy P . _)
               (when (<= P PP)
                 (.. (t) (term New P))	 
                 (.. (rterm (list p New N M) To PP P))))

              (_ <fail>)))

           (<and>
            (.. (c) (ws '()))
            (.. (t) (rest-atom '()))
            (.. (rterm t To PP 0))))))
      
     
      (<p-define> (rterm Ti To PP L)
        (<var> (T)
          (<or>
           (<and>
            (.. (c) (ws '()))
            (.. (p) (fop2 '()))
            (match p
              (('xfx P . _)
               (when (and (<= P PP) (< L P))
                 (.. (t) (term T (- P 1)))
                 (.. (rterm (list p Ti T N M) To PP P))))
              
              (('xfy P . _)
               (when (and (<= P PP) (< L P))
                 (.. (t) (term T P))
                 (.. (rterm (list p Ti T N M) To PP P))))

              (('yfx P . _)
               (when (and (<= P PP) (<= L P))
                 (.. (t) (term T (- P 1)))
                 (.. (rterm (list p Ti T N M) To PP P))))

              (('yf P . _)
               (when (and (<= P PP) (<= L P))
                 (.. (rterm (list p Ti N M) To PP P))))

              (('xf P . _)
               (when (and (<= P PP) (< L P))
                 (.. (rterm (list p Ti N M) To PP P))))

              (_ <fail>)))
           (<and>
            (<=> Ti To)
            (<p-cc> To)))))
      
      (define (expr N)
        (<p-lambda> (c)
          (<var> (T)
            (.. (x) (term T N))
            (<p-cc> (scm T)))))

      expr)))

(generate-operator-tool mk-operator-expression <p-define> <p-lambda> .. <p-cc>)
