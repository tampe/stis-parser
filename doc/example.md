Let's make a parser for a small calculator language. We will make scanner
part as well as the parser part in the same framework. Lets define a number

   ::::scheme
   (define numseq (f* (f-reg! "[0-9]")))
   (define pm     (f? (f-reg! "[-+]")))
   (define int    (f-seq pm numseq))
      
   (define dec (f-seq (f? int) (f-char ".") (f? numseq))
   (define decimal
      (p-freeze 'decimal (mk-token dec)
         (lambda (s c str) (string->number str))))

   (define var (f-seq (f-reg! "[_a-zA-Z]") (f* (f-reg! "[_0-9a-zA-Z]"))))
   (define variable
   	   (p-freeze 'variable (mk-token var)
	      (lambda (s c str) (string->symbol str))))

   (define hex (f-seq (f-str "0x") (f+ (f-reg! "[0-9aAbBcCdDeEfF]"))))

   (define integer
      (p-freeze 'integer
        (mk-token (f-or! hex int))
        (lambda (s c out-string)
            (string->number out-string))))


   (define b-e   (f-or! mul-1 div-e))
   (define mul-e (f-list #:mul a-e ws (f-reg "*") ws a-e))
   (define div-e (f-list #:div a-e ws (f-reg "/") ws a-e))
		      	    	 
           

     